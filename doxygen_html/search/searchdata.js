var indexSectionsWithContent =
{
  0: "abcdefghijklmnoprstuvwxyz~",
  1: "abcempt",
  2: "acdemp",
  3: "abcdeghimprstuvw~",
  4: "abcdefghijklmnoprst",
  5: "ct",
  6: "bcgmsxyz",
  7: "e"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "related"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Friends"
};

