# run with 
#docker build --network=host -t lorimax/path_planning .


#FROM moveit/moveit:noetic-release
FROM moveit/moveit:noetic-release
#FROM ubuntu:focal
#ARG USER=user
ENV DEBIAN_FRONTEND=noninteractive
ENV ROS_DISTRO=noetic
ENV ament_cmake_DIR=/opt/ros/$ROS_DISTRO/share/ament_cmake/cmake

# Update the package lists and install necessary dependencies
RUN apt-get update && apt-get install -y \
    curl \
    gnupg2 \
    lsb-release \
    locales \
    sudo \
    git \
    wget 
WORKDIR /ros_ws
COPY . .
RUN apt-get update
RUN apt update
#RUN apt-get install -y ros-noetic-desktop-full
RUN apt-get install -y ros-noetic-ros-base && apt-get install -y \
    ros-noetic-pcl-ros \
    ros-noetic-catkin python3-catkin-tools \
    ros-noetic-rviz-visual-tools \
    ros-noetic-moveit-visual-tools \
    ros-noetic-tf-conversions
RUN apt-get update
RUN /bin/bash -c "source /opt/ros/noetic/setup.bash; catkin build"
RUN /bin/bash -c "source devel/setup.bash"
