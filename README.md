# Path Plannning

To see a detailed documentation of the code, have a look at the Doxygen documentation found in `doxygen_html`.
Just open `doxygen_html/index.html` with your browser and click through the documentation.

If you want to update the documentation after changing things, don't forget to update the OUTPUT_DIRECTORY tag in the Doxyfile.

<!-- TOC start -->
## Table of contents

- [Getting started](#getting_started)
    * [Requirements](#requirements)
    * [Local setup](#local_setup)
    * [Deploy changes](#deploy_changes)
- [Environment](#environment)
- [Goal](#goal)
- [Technology](#technology)
- [Benchmarking](#benchmarking)
  * [Benchmarking problems](#benchmarking-problems)
  * [Results](#results)
    + [Projections](#projections)
      - [Successfull Runs](#successfull-runs)
      - [Time](#time)
      - [Trajectory Length](#trajectory-length)
    + [Benchmarking Planners](#benchmarking-planners)
      - [Ranking Planners](#ranking-planners)
      - [Investigating the influence of planning time](#investigating-the-influence-of-planning-time)
- [Future work and ideas](#future-work-and-ideas)
  * [Post processing](#post-processing)
  * [Optimizing global trajectories](#optimizing-global-trajectories)
    + [Background](#background)
    + [Idea](#idea)
    + [Implementation](#implementation)
  * [Trajectory and target database](#trajectory-and-target-database)

<!-- TOC end -->

<!-- TOC --><a name="getting_started"></a>
## Getting Started

This chapter guides you through the set up of the path planning component on your local system and shows you how to execute a simple example.
Note: This is meant for developement of the path planning component. If you just want to use it, you can of course just pull the `lorimax/path_planning:latest` image form Docker Hub and run it in a container.

<!-- TOC --><a name="requirements"></a>
### Requirements

Make sure you have ROS Noetic installed or [install it.](http://wiki.ros.org/noetic/Installation/Ubuntu)

Make suer you have MoveIt1 installed or install it with:

    sudo apt install ros-noetic-moveit

You may need to manually install some other packages:

    sudo apt-get install ros-noetic-catkin python3-catkin-tools 
    sudo apt-get install ros-noetic-rviz-visual-tools
    sudo apt-get install ros-noetic-moveit-visual-tools

<!-- TOC --><a name="local_setup"></a>
### Local setup

Clone the repository into youre preferred destination.

Inside `path_planning/src` you see the directories `universal_robot` and `robotiq_2finger_grippers`. They contain resources about the robot and the gripper.
Replace those empty directories with the respective cloned repositories:

    git clone https://github.com/Danfoa/robotiq_2finger_grippers.git
    git clone https://github.com/ros-industrial/universal_robot.git

Now navigate back to `path_planning` and source ROS:

    source /opt/ros/noetic/setup.bash

Build the component with:

    catkin build

And source it with:

    source devel/setup.bash

Now you should be all set.

<!-- TOC --><a name="example"></a>
### Path Planning Example

We will now set up a simple working example. It consists of two ROS nodes: the `path_planning` node and the `dummy_action_client`.

The latter is only used for this example and will mimic another component communicating with the path_planning component.

Path planning is devided into two ROS Actions: `Assemble_Configuration` and `Plan`. The action `Assemble_Configuration` receives information about the robots environment and creates a planning scene for further path planning. The `Plan` action accepts a start state (joint angle values) for the robot and a list of targets which my be end effector poses or robot states (vector of joint angle values). This is the part that actually does the planning and returns the planned trajecotry. `path_planning` connects the two actions and defines the ROS node that runs them.

We will use the `dummy_action_client` to send requests to the `path_planning` node that will build some configuration and plan some trajectories.

**NOTE** For the `dummy_action_client` to work properly please make sure to change any paths in it to the respective paths on your system.

Run Path Planning Component:
    
    source devel/setup.bash
    roslaunch handling_unit_moveit_config path_planning.launch

Run Dummy Action Client:
    
    source devel/setup.bash
    roslaunch handling_unit dummy_action_client.launch

### Tips and Tricks

When planning [MoveIt plans in the robots joint space and considers multiple possible joint space targets resulting from one pose target.](https://answers.ros.org/question/295756/in-what-space-does-moveit-plan-paths/ )

To get the current end effector pose from rviz, open a new terminal and run:

    rostopic echo /rviz_moveit_motion_planning_display/robot_interaction_interactive_marker_topic/feedback


When running the component inside a container in the AFS_Remastered context, changing the robots virtual environment normaly involves changing its physical environment which can be quite difficult. You can circumvent this by attaching a shell to the `path_planning` container (for example through VS Code) and manually sending an `Assemble_configurationActionGoal` to the `path_planning` node. See the example below for the exact syntax:

    source devel/setup.bash
    rostopic pub assemble_configuration_action_server/goal path_planning_ros_interface/Assemble_configurationActionGoal '{header: auto, goal: {configuration: "{\"modules\": [{\"module_id\": \"pressing_unit\", \"3d_mesh\": \"/ros_ws/src/descriptions/units/auspresseinheit.dae\", \"operation\": \"UPDATE\", \"pose\": {\"plate\": {\"x\": 1, \"y\": 1}, \"orientation\": 2}}]}"}}'

This example changes the position and orientation of the `pressing_unit` module.

**ATTENTION** While this may be very useful sometimes, this action does disconnect the robots virtual environment from its physical environment which can lead to unrecognized collisions. Pleas only use this with caution!

<!-- TOC --><a name="deploy_changes"></a>
### Deploy Changes
After changing something, make sure to deploy your changes by creating a new docker image and pushing it to Docker Hub. You can do so by:

    docker build -t lorimax/path_planning:latest .
    docker push lorimax/path_planning

Please change the Docker Hub Repository to a different one since this is currently my private one. Please update this documentation and the Dockerfiles in AFS_Remastered afterwards.
Now don't forget to replace the old image wherever you want to use the new one.


<!-- TOC --><a name="environment"></a>
## Environment

The system is a 6 DoF robotic arm of the type [Universal Robots UR10e](https://www.universal-robots.com/de/produkte/ur10-roboter/?utm_source=Google&utm_medium=cpc&utm_cja=Expert&utm_leadsource=Paid%20Search&utm_campaign=HQ_DE_Always-On2021&utm_content=textad&utm_term=ur10e&gad=1&gclid=EAIaIQobChMI6s-y8-jl_wIVl-F3Ch1qUAE0EAAYASAAEgIu1PD_BwE) mounted on the so called **station C**.

The station consist of tables (1m wide, 1m long) in certain positions. The number of tables, as well as their position might be reconfigured at some point. A table holds space for up to 4 base plates (0.5m wide, 0.5m long). Those base plates are building blocks for a reconfigurable production environmeant. Meaning, each base plate provides the base for the mount of different machines or other physical objects of the production process, called modules.The types of modules are not defined, additional mountable modules might be developed later on and thus are currently unknown.
By arranging base plates with modules around the robot, a production environment can be created.

In the current configuration, station C consists of two tables. On one table, the robot is located along with two machines and one base plate that is interpreted as storage space and thus called **storage**. The machines are called **magnet_extraction_unit** and **pressing_unit** while the robot is referred to as **handling_unit**. They are arranged as illustrated in the picture below.

![Current configuration of station c. Handling_unit North-West, pressing_unit North-East, magnet_extraction_unit South-East, storage South-West](media/configuration_station_c.png)

The second table can be disregarded for now.

<!-- TOC --><a name="goal"></a>
## Goal

In this deliberately reconfigurable environment, the handling_unit must perform placing task such as feeding machines, extracting objects from machines, moveing objects between base plates, etc. and thus perform an automized production process.
This process as well as the entire configuration is planned by higher components according to the needs and optimization wishes of an abstract client.
The component developped here is responsible for the movements of the handling_unit. That includes supporting the planning process by providing information about movements as well as planning and optimizing movements of the handling_unit. The focus is first set on the first two tasks.

This component will receive the exact task as well as information about it from a higher component and will respond. More specifically, the task that will be considered first will be:

**Plan an optimized movement in the form of a list of consecutive joint angles that iteratively reaches target positions**, provided a list of desired targets, the start state of the robot and information about the current configuration of the station. The optimization objective is supposed to be minimal movement but may be different.

<!-- TOC --><a name="technology"></a>
## Technology

The communication is done via [ROS](http://wiki.ros.org/ROS/Introduction) [noetic](http://wiki.ros.org/noetic). To work with the handling_unit [MoveIt! 1](https://ros-planning.github.io/moveit_tutorials/) is used.

For path planning, [OMPL](https://ompl.kavrakilab.org/) is used through moveit. We use  [BiTRRT](https://ieeexplore.ieee.org/document/6631158), [RRT*](https://arxiv.org/pdf/1105.1186.pdf) and [PRM*](https://arxiv.org/pdf/1105.1186.pdf) to simultaniously plan a trajectory and then take the shortest one.

<!-- TOC --><a name="benchmarking"></a>
## Benchmarking

Since OMPL offers an array of planning algorithms, we had to select some of them to use for planning. Since the planning problems are not known beforehand and arise through the different applications and environments the handling unit might face, planners that reliably produce good solutions have to be identified.

To this end, we set up a benchmarking environment and run a benchmark on various planners offered by OMPL. Then, we developped a scoring system and selected the most suitable planners. In this chapter you can find a description of the benchmarking process along with an overview of our results and reasoning.

To perform the two tasks that were set, we sought for:

1. Planners that are fast and reliable. Those planners regularly find solutions to diverse planning problems and only rarely fail to find a solution. They do so in very short time while not only the mean time shall be short but also the spread of time shall be narrow. Meaning, not only do they find a solution quickly on average, but also yield a very low probability to take a long time to find a soltion on a given problem.
2. Planners that are reliable and offer high quality solutions. Those planners also rarely fail to find a solution. But the solutions they find are also of a high quality. Path quality is defined by 6 aspects.
    1. Average length of the solution trajectory in the robots joint space.
    2. Spread of the trajectory length in the joint space.
    3. Average weighted length of the solution trajectory in joint space. This weighted length puts factors on the last three joints of the robot to punish excessive movemet in those joints (for example unnecessarily spinning the end effector). 
    4. Spread of the weighted lenght in joint space.
    5. Average distance covered by the end effector in cartesian space.
    6. Spread of the distance covered by the end effector in cartesian space.

<!-- TOC --><a name="benchmarking-problems"></a>
### Benchmarking problems

The benchmark environment consists of 4 different configurations of Magneteinheit, Auspresseinheit and Storage to obtain results in different environments.
Below the environment and the paths planned in the benchmark are visualized.
![alt text](media/benchmark_path.png)

First, the robot moves into a start state. Then it moves to:
- Magneteinheit A
- Storage A
- Auspresseinheit A
- Magneteinheit B
- Storage B
- Auspresseinheit B
- Magneteinheit C
- Storage C
- Auspresseinheit C
- Magneteinheit D
- Storage D
- Auspresseinheit D

Each of those 12 problems is planned by all planners 150 times while the maxmimum available planning time was 5 seconds. Other planner parameters are at their default values as testing each planner for a number of different values in each parameter would be very tedious. After each run, data about the result is collected. That is:
- Whether or not the problem was successfully solved
- Trajectory Length in joint space
- Trajectory Length in cartesian space (end effector movement)
- Smoothnes
- Time until solution was found
- Weighted length in joint space
- The covered distance of each joint in joint space

This data is stored for each problem in a .csv file. Additionally, the resulting trajecotries are stored so that they can be checked when needed.

<!-- TOC --><a name="results"></a>
### Results

<!-- TOC --><a name="projections"></a>
#### Projections

Some planners use projections to determine undiscovered parts of the configuration space. They project the high dimensional configuration space onto a lower dimensional space in which they track coverage. This projection influences the planners performance and can be chosen aribtrarily.
However, it is common (afaik) to chose the space of the first joints of the robot. To correctly set up the planners, we tested two projections:

joints_1_2:
- shoulder_pan_joint
- shoulder_lift_joint

joints_1_2_3
- shoulder_pan_joint
- shoulder_lift_joint
- elbow_joint

<!-- TOC --><a name="successfull-runs"></a>
##### Successfull Runs

With the projection joints_1_2_3 the planner LBKPIECE was able to find solutions only 120-130 times out of 150 runs. Additionally, the planners KPIECE and ProjEST experienced difficulties with Magneteinheit B, also only finding 130 out of 150 solutions.


<!-- TOC --><a name="time"></a>
##### Time

With joints_1_2_3 BKPIECE and especially LBKPIECE took significantly longer to find a solution (BKPIECE average 1sec, LBKPIECE average 3sec).
With joints_1_2 all planners found their solutions in less than 1sec.

With both projections, KPIECE and ProjEST had significant spread with Auspresseinheit C as well as with Magnetieinheit C and Magneteinheit B. The latter being worse with joints_1_2

<!-- TOC --><a name="trajectory-length"></a>
##### Trajectory Length

The projections joints_1_2 and joints_1_2_3 yield similar results with all planners. The spread is higher higher with joints_1_2_3


Result: as joints_1_2 is faster and securer while not yielding worse solutions we prefer joints_1_2 ofer joints_1_2_3 and will continue with that projection.

<!-- TOC --><a name="benchmarking-planners"></a>
#### Benchmarking Planners

The main benchmarking task involved 22 planners, namely: BiTRRT, RRTConnect, BiEST, LazyPRM, PRM, BKPIECE, BFMT, SBL, FMT, EST, ProjEST, KPIECE, RRT, STRIDE, LBKPIECE, TRRT, LazyPRMstar, PRMstar, SPARS, RRTstar, LBTRRT and SPARStwo. 

See [OMPL](https://ompl.kavrakilab.org/planners.html) for more information. 

<!-- TOC --><a name="ranking-planners"></a>
##### Ranking Planners

To select the best planners for each task, a score was developed to rank the planners. For each considered category and each problem, a planner can score points. The points in each category can be weighted to emphasize certain categories. With a weight of 1, a planner can score between 0 and 22 points in each category for each problem. The points depend on the relative performance of the planner. 

Below, the formula to calculate the points of a planner in a category is given. $s$ being the scored points, $w$ the weight of the category, $v_b$ the best value of all planners in this category, $v_w$ the worst value of all planners in this category and $x$ being the value this planner achieved.


$$
s = w(22 - 22{\lvert v_b - x\rvert \over \lvert v_b + v_w\rvert})
$$

As can be seen, an $x$ that is close to $v_b$ yields higher points. At the same time, in a category, in which the planners performance is diverse, leading to a greater distance between $v_b$ and $v_w$, an $x$ close to $v_b$ will yield more points than in a category in which $v_b$ and $v_w$ are close.

Additionally, a planner can score extra points if it manages to regularly find a solution that is within the best solutions for a problem according to a certain category. For example, if a planner finds a solution that is within the top 3 solutions considering trajectory length in joint space for a problem, this planner gains extra points. 

To that end, the solutions are ranked for a specific category and a specific problem and the planners that caluclated the best solutions are determined. A weight can be put on the scored points for a specific category as well.

Below the formular to calculate those points for a certain problem and a certain category is given. $w$ being the weight of this category, $n_t$ being the number of best solutions that is considered (top 10 solutions --> $n_t = 10$) and $a$ being the number of appearances of solutions from this planner in the top $n_t$ solutions and $s$ being the score.

$$
s = w({22a \over n_t})
$$

For each of the two tasks that we consider, a separate score was calculated. The different scores included the following categories:

1. Score Speed
    1. Number of successfull runs ($w = 1$)
    2. Mean planning time ($w = 1$)
    3. Planning time spread ($w  =0.5$)
2. Score Quality
    1. Number of successfull runs ($w = 1$)
    2. Mean trajectory length in joint space ($w = 1$)
    3. Trajectory length spread in joint space ($w = 1$)
    4. Mean weighted length in joint space ($w = 0.5$)
    5. Weighted length spread in joint space ($w = 0.5$)
    6. Mean trajectory length in cartesian space ($w = 0.3$)
    7. Trajectory length spread in cartesian space ($w = 0.3$)
    8. Appearances in top 3 solutions for trajectory length in joint space ($w = 1$)
    9. Appearances in top 3 solutions for weighted trajectory length in joint space ($w = 0.5$)
    10. Appearances in top 3 solutios for trajectory length in cartesian space ($w = 0.3$)


Applying this, we obtained the following ranking for Score Speed and Score Quality:

|Rank|Planner|Score Speed||Rank|Planner|Score Quality|
|---|---|---|---|---|---|---|
|1  |BiTRRT|636||1|RRTstar|979|
|2  |RRTConnect|629||2|FMT|942|
|3  |BiEST|626||3|PRMstar|926|
|4  |LazyPRM|617||4|BFMT|906|
|5  |PRM|606||5|SPARStwo|894
|6  |BKPIECE|597||6|LBTRRT|867|
|7  |BFMT|583||7|LazyPRMstar|864|
|8  |SBL|583||8|TRRT|849|
|9  |FMT|577||9|BiTRRT|819|
|10 |EST|572||10|LazyPRM|811|
|11 |ProjEST|571||11|EST|797|
|12 |KPIECE|570||12|SPARS|776|
|13 |RRT|564||13|RRTConnect|773|
|14 |STRIDE|555||14|BKPIECE|773|
|15 |LBKPIECE|551||15|PRM|763|
|16 |TRRT|533||16|LBKPIECE|757|
|17 |LazyPRMstar|389||17|STRIDE|756|
|18 |PRMstar|384||18|RRT|755|
|19 |SPARS|369||19|KPIECE|754|
|20 |RRTstar|363||20|SBL|750|
|21 |LBTRRT|363||21|ProjEST|747|
|22 |SPARStwo|352||22|626|738|

With these results, we implemented BiTRRT, RRTConnect and BiEST as the planners for quickly generating valid solutions (task 1).

<!-- TOC --><a name="investigating-the-influence-of-planning-time"></a>
##### Investigating the influence of planning time

Some planning algorithms are optimizing. Meaning, they try to optimize their trajectory with respect to a certain objective. In our case, as is the standard, this objective is minimal joint space distance. Therefore, those planners continue optimizing after they have found a trajectory until they reach a set time limit. In the previous benchmarks, this limit was set to 3 sec but in practice, more time might be available. So it seems to be worthwile to investigate how longer planning times may set the optimizing planners apart from the non-optimizing.

To that end, we chose to further investigate the performance of RRTstar, FMT, PRMstar, BFMT, LBTRRT, LazyPRMstar and TRRT. SPARStwo was left out because it only yielded very few successfull runs. 

The optimizing planners are the ones ending with "star" as a reference to the A* algoritm they use for optimization.

We reduced the benchmarking problem to:
- Move to Magneteinheit A
- Move to Storage A
- Move to Auspresseinheit A

We ran 50 iterations on each problem with each of the seven planners. Then, we increased the available planning time and repeated the experiment. 
The planning time was discretized in steps of 5 seconds ranging from 5 seconds to 200 seconds. Because of the long duration of the experiment (several days) and because of it yielding good results before reaching its finish, we aborted the experiment at 165 seconds of planning time.

Below, we show the median distance in the joint space of the trajectories calculated by each planner as well as the respective spread over the planning times for one problem.

![alt text](media/planning_time_results_auspress_A.png)

As can be seen, the median distance as well as the spread show now significant improvement over the planning time.
Based on this data, we decided to continue with RRTstar, FMT and PRMstar for task 2.

<!-- TOC --><a name="future-work-and-ideas"></a>
## Future work and ideas

<!-- TOC --><a name="post-processing"></a>
### Post processing

MoveIt! does not only offer path planning algorithms from OMPL but also algorithms that can take an existing trajectory and optimize it as a post processing step. Since an increased planning time did not yield better trajectories, post processing might be interesting to look into.

MoveIt! offers namely the [STOMP](http://wiki.ros.org.jsk.imi.i.u-tokyo.ac.jp/attachments/Papers(2f)ICRA2011_Kalakrishnan/kalakrishnan_icra2011.pdf) planner and the [CHOMP](https://journals.sagepub.com/doi/10.1177/0278364913488805) planner for post processing.

Information on how to implement post processing can be found here:

- [Information on how to implement STOMP in MoveIt!](https://ros-planning.github.io/moveit_tutorials/doc/stomp_planner/stomp_planner_tutorial.html)
- [Information on how to implement CHOMP in MoveIt!](https://ros-planning.github.io/moveit_tutorials/doc/chomp_planner/chomp_planner_tutorial.html)
- [Running STOMP or CHOMP as post processors for OMPL](https://ros-planning.github.io/moveit_tutorials/doc/planning_adapters/planning_adapters_tutorial.html)

<!-- TOC --><a name="optimizing-global-trajectories"></a>
### Optimizing global trajectories

As we operate in the context of a production process, we expect to receive various planning requests, that are connected to the same cyclic process. If various trajectories are to be executed one after the other in a reappearing process, it might be advantageus to not only optimize the individual trajectory but the whole process.

<!-- TOC --><a name="background"></a>
#### Background

For example, let's assume that a point in cartesian space $A$ corresponds to a set of points in joint space $a_1, ..., a_n$ (due to the higher dimensionality of the joint space). If we plan from point $A$ to a point $B$ we might get an optimal (in terms of minimal distance in joint space) trajectory $t_1$ that connects the points $a_3$ and $b_5$ in joint space. After that, we want to move to a point $C$. Since our robot is now at configuration $b_5$ we have to start there and maybe get a trajectory $t_2$ that leads to $c_1$ in joint space. 

Both trajectories, $t_1$ and $t_2$, may be optimal in their specific context. But the optimal trajectory for a movement from point $A$ to point $C$ over point $B$ may be a different one and may actually connect the points $a_1, b_2$ and $c_3$ in joint space. We could not find that solution because we were only locally solving and optimizing the planning problem.

<!-- TOC --><a name="idea"></a>
#### Idea

To implement a global approach to finding good trajectories, we need to consider various targets in joint space for each target in cartesian space. A point in cartesian space may correspond to no point, one point or a (possibly infinite) number of points in joint space. Mathematically we have to allow infinite points as a possibility but we can not realize infinitely small movements with the robot and also want to discretize the joint space. In any case, we need to find a sufficiently large set of joint space points for each cartesian goal, should such points exist.

The strategy then would be to build a layered graph in which the nodes in each layer represent the points in joint space that correspond to one goal in cartesian space. The nodes in each layer are connected to the nodes in the next layer by edges that represent trajectories, connecting the two points in joint space. The cost of the edge is the trajectory length in join space. Now we can implement algorithms (maybe dynamic programming) to calculate the cheapest (shortest) route through the graph. The edges in that route are then our trajectories.

It is expected that this results in graphs with a high number of edges. To calculate the cost for each edge, we need to solve and optimize a planning problem for each edge which yields high computation costs. A work around can therefore be to not assign the length of the optimal trajectory between to points as cost to an edge but the euklidean distance in joint space between those points. The euklidean distance can be calculated very easily and quickly. Now, in this graph, we don't take only the cheapest route because we were only considering euklidean distance and did not yet include actual collision free trajectories. But we chose the cheapest 3 or 10 or something routes. Then, we calculate the trajectories only for those routes and compare them again and now choose the cheapest. That way we may be able to significantly reduce computational cost due to not having to calculate the optimal trajectory for each edge. Although we might not be able to find the actual cheapest route, we still might find one that is significantly better than only optimizing locally.

<!-- TOC --><a name="implementation"></a>
#### Implementation

For the first step, we need an inverse kinematics solver that can produce more than one solution in joint space. The [default IK solver from MoveIt! (KDL)](https://ros-planning.github.io/moveit_tutorials/doc/kinematics_configuration/kinematics_configuration_tutorial.html) can only give one solution.

We can, however, query the KDL solver multiple times with different (random) seeds. This approach was mentioned [here](https://groups.google.com/g/moveit-users/c/dUkNAXSTXsc?pli=1) and apparently was used for the moveit implementation of Descartes. Regard the code of Descartes for [getting multiple IK solutions with KDL](https://github.com/ros-industrial-consortium/descartes/blob/indigo-devel/descartes_moveit/src/moveit_state_adapter.cpp#L171-L219) and for [searching for seed states](https://github.com/ros-industrial-consortium/descartes/blob/hydro-devel/descartes_moveit/src/seed_search.cpp). We found this solution to not be sufficient as we don't know the current relationship between the seed and the KDL's solution and had to query the KDL solver many times to get sufficiently different solutions which took a long time and is quite inefficient.

A solver that should be able to produce multiple solutions is [IK Fast](https://ros-planning.github.io/moveit_tutorials/doc/ikfast/ikfast_tutorial.html). This solver analyzes the kinematic chain of the robot and produces C++ code through which the solutions to an inverse kinematics problem can be obtained. IK Fast can be included in MoveIt! but the production of the C++ files requires specific libraries in specific versions and is not easy. To facilitate its use, MoveIt! offers a docker container that provides the necessary requirements. IK Fast is then to be run inside that container. We tried to include IK Fast that way but encountered problems because the urdf files of our robot refer to files with 3D meshes that are located on the system and have to be included in the docker container. We didn't continue this because of more pressing concerns at the time.

<!-- TOC --><a name="trajectory-and-target-database"></a>
### Trajectory and target database

In our production environment there is a finite number of possible modules that may be used. Each module offers specific funcitonality and therefore requieres specific manipulation resulting in a finite set of tasks that may occur when working with this module. This results in movements, that are specific to that module.

We can use this to our advantage. We know, which moules may appear and what tasks may have to be performed with those moudules and therefore which positions have to be reached inside the coordinate frame of those modules.

We can store this information in a database and receive only planning requests that indicate us which of the stored positions we should use. Receiving and processing such requests is already implemented (key points). We can receive a module id and a position in the modules coordinate frame or a key point id and calculate a trajectory with that. But we don't have a database yet and have not yet formalized the different positions for each module. 

Apart from positions, we could also consider cartesian trajectories consisting of waypoints for each module. For example, placing an object in the module requires following a specific cartesian trajectory. By having these predefined cartesian trajecotries available, we may be able to generate more precise movements, better avoid collisions and solve planning problems faster because we are guided in complicated areas.

The idea would be, that the module itself is intelligent has its own storage with its specific positions and trajecotries. The module would then only log into the station and offer its information in a network or upload it on a database.

