{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://example.com/product.schema.json",
    "title": "Planning Reqeust",
    "description": "Constitutes a request to the plan_action_server to plan trajectories to defined targets.",
    "type": "object",
    "properties": {
        "start_state": {
            "description": "The joint angle state from which the plan_action_server should plan.",
            "type": "object",
            "properties": {
                "shoulder_pan_joint": {
                    "description": "Start angle of the shoulder pan joint.",
                    "type": "number"
                },
                "shoulder_lift_joint": {
                    "description": "Start angle of the shoulder lift joint.",
                    "type": "number"
                },
                "elbow_joint": {
                    "description": "Start angle of the elbow joint.",
                    "type": "number"
                },
                "wrist_1_joint": {
                    "description": "Start angle of the wrist 1 joint.",
                    "type": "number"
                },
                "wrist_2_joint": {
                    "description": "Start angle of the wrist 2 joint.",
                    "type": "number"
                },
                "wrist_3_joint": {
                    "description": "Start angle of the wrist 3 joint.",
                    "type": "number"
                }
            },
            "required": ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        },
        "targets": {
            "description": "The planning targest. Planning will be done in this order, considering the previous target as the starting point for the next target.",
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "type": {
                        "description": "Specifier for the format of the target. May be GLOBAL_POSE, JOINT_STATE or MODULE_POSE",
                        "type": "string",
                        "enum": ["GLOBAL_POSE", "JOINT_STATE", "MODULE_POSE"]
                    },
                    "gripper_state": {
                        "description": "State (i.e. angle) of the finger joint in the gripper.",
                        "type": "number"
                    },
                    "object_gripped": {
                        "description": "Determines whether collision checking should consider an object being gripped by the robot.",
                        "type": "boolean"
                    },
                    "position": {
                        "description": "Position of the target pose.",
                        "type": "object",
                        "properties": {
                            "x": {
                                "description": "Position on the x axis.",
                                "type": "number"
                            },
                            "y": {
                                "description": "Position on the y axis.",
                                "type": "number"
                            },
                            "z": {
                                "description": "Position on the z axis.",
                                "type": "number"
                            }
                        },
                        "required": ["x", "y", "z"]
                    },
                    "orientation": {
                        "description": "Orientation quaternion of the target pose.",
                        "type": "object",
                        "properties": {
                            "x": {
                                "description": "x value of the orientation quaterion",
                                "type": "number"
                            },
                            "y": {
                                "description": "y value of the orientation quaterion",
                                "type": "number"
                            },
                            "z": {
                                "description": "z value of the orientation quaterion",
                                "type": "number"
                            },
                            "w": {
                                "description": "w value of the orientation quaterion",
                                "type": "number"
                            }
                        },
                        "required": ["x", "y", "z", "w"]
                    },
                    "values": {
                        "description": "Joint angle values as target.",
                        "type": "object",
                        "properties": {
                            "shoulder_pan_joint": {
                                "description": "Angle of the shoulder pan joint.",
                                "type": "number"
                            },
                            "shoulder_lift_joint": {
                                "description": "Angle of the shoulder lift joint.",
                                "type": "number"
                            },
                            "elbow_joint": {
                                "description": "Angle of the elbow joint.",
                                "type": "number"
                            },
                            "wrist_1_joint": {
                                "description": "Angle of the wrist 1 joint.",
                                "type": "number"
                            },
                            "wrist_2_joint": {
                                "description": "Angle of the wrist 2 joint.",
                                "type": "number"
                            },
                            "wrist_3_joint": {
                                "description": "Angle of the wrist 3 joint.",
                                "type": "number"
                            }
                        },
                        "required": ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
                    },
                    "module_id": {
                        "description": "ID of the module in which the following pose target or key point target is defined.",
                        "type": "string"
                    },
                    "point_id": {
                        "description": "Use a key point of the module defined by module_id as target.",
                        "type": "integer",
                        "minimum": 0
                    },
                    "linear_path": {
                        "description": "Specifies whether or not to plan a straight linear path.",
                        "type": "boolean"
                    }
                },
                "required": ["type", "gripper_state", "object_gripped"],
                "dependentRequired": {
                    "point_id": ["module_id"],
                    "position": ["orientation"],
                    "orientation": ["position"]
                },
                "anyOf": [
                    {"required": ["point_id"]},
                    {"required": ["values"]},
                    {"required": ["position"]},
                    {"required": ["orientation"]}
                ]
            },
            "minItems": 1
        }
    },
    "required": ["targets"]
}