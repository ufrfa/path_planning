{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://example.com/product.schema.json",
    "title": "Assemble Configuration",
    "description": "Constitutes a request to the plan_action_server to change the current configuration.",
    "type": "object",
    "properties": {
        "robot": {
            "description": "Transformation from the robot base coordinate frame to the station coordinate frame, which is located in the corner of the first table. The robot does not usually sit exactly on the corner.",
            "type": "object",
            "properties": {
                "position": {
                    "description": "Position of the robot base coordinate frame in the station coordinate frame.",
                    "type": "object",
                    "properties": {
                        "x": {
                            "type": "number"
                        },
                        "y": {
                            "type": "number"
                        }
                    },
                    "required": ["x", "y"]
                }
            },
            "required": ["position"]
        },
        "table_config": {
            "description": "Array of tables that constitute the station. Each table consists of 4 base plates. There is always a table with id 0 at (0,0).",
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "description": "id of the new table",
                        "type": "integer",
                        "minimum": 1
                    },
                    "position": {
                        "description": "Position of the table in the station coordinate frame.",
                        "type": "object",
                        "properties": {
                            "x": {
                                "type": "integer"
                            },
                            "y": {
                                "type": "integer"
                            }
                        },
                        "required": ["x", "y"]
                    }
                }
            }
        },
        "modules": {
            "description": "Array of modules that may be added, removed or updated.",
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "module_id": {
                        "description": "id of the module",
                        "type": "string"
                    },
                    "3d_mesh": {
                        "description": "URL to the .dae file that contains the 3d collision model of the module. You may start with http:// or file:// or package:// etc. See https://wiki.ros.org/resource_retriever",
                        "type": "string"
                    },
                    "pose": {
                        "description": "Position and orientation of the module expressed as position of the base plate and orientation aroudn z-axis.",
                        "type": "object",
                        "properties": {
                            "plate": {
                                "description": "Position of the base plate on which this module is located.",
                                "type": "object",
                                "properties": {
                                    "x": {
                                        "type": "integer"
                                    },
                                    "y": {
                                        "type": "integer"
                                    }
                                },
                                "required": ["x", "y"]
                                },
                                "orientation": {
                                    "description": "Orientation around the z-axis. Since only steps of 90 degrees are possible, this is expressed through integers 0, 1, 2, 3 where each of them represent a 90 degree turn.",
                                    "type": "integer",
                                    "minimum": 0,
                                    "maximum": 3
                                }
                        },
                        "required": ["plate", "orientation"]
                    },
                    "key_points": {
                        "description": "Array of key points in the module coordinate frame that can be referenced later.",
                        "type": "array",
                        "items": {
                            "type": "object",
                            "properties": {
                                "id": {
                                    "description": "id of the key point.",
                                    "type": "integer",
                                    "minimum": 0
                                },
                                "position": {
                                    "description": "Positon of the key point in the module coordinate frame.",
                                    "type": "object",
                                    "properties": {
                                        "x": {
                                            "type": "number"
                                        },
                                        "y": {
                                            "type": "number"
                                        },
                                        "z": {
                                            "type": "number"
                                        }
                                    },
                                    "required": ["x", "y", "z"]
                                },
                                "orientation": {
                                    "description": "Orientation quaternion of the end effector in the module coordinate frame that is associated with this key point.",
                                    "type": "object",
                                    "properties": {
                                        "x": {
                                            "type": "number"
                                        },
                                        "y": {
                                            "type": "number"
                                        },
                                        "z": {
                                            "type": "number"
                                        },
                                        "w": {
                                            "type": "number"
                                        }
                                    },
                                    "required": ["x", "y", "z", "w"]
                                }
                            },
                            "required": ["id", "position", "orientation"]
                        }
                    },
                    "operation": {
                        "description": "Specifier for the operation that should be performed with this module.",
                        "type": "string",
                        "enum": ["ADD", "REMOVE", "UPDATE"]
                    }
                },
                "required": ["module_id", "operation"]
            }
        }
    },
    "anyOf": [
        {"required": ["table_config"]},
        {"required": ["modules"]},
        {"required": ["robot"]}
    ]
}