#ifndef MODULE_H
#define MODULE_H

#include <string>
#include <vector>
#include <tf2/LinearMath/Quaternion.h>
#include <ros/ros.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include "geometric_shapes/shapes.h"
#include "geometric_shapes/mesh_operations.h"
#include "geometric_shapes/shape_operations.h"
#include <map>
#include <Eigen/Dense>
#include <math.h>
#include <stdexcept>

enum CPVShapes{
    BOX, SPHERE, CYLINDER, CONE
};

enum CPVBox{
    X, Y, Z
};

enum CPVSphere{
    SPHERE_RADIUS
};

enum CPVCylinder{
    CYLINDER_HEIGHT, CYLINDER_RADIUS
};

enum CPVCone{
    CONE_HEIGHT, CONE_RADIUS
};

/**
 * @brief The struct defining Collision Protection Volumina (CPVs).
 *
 * A lot of the modules used are not well represented by their 3D mesh.
 * This may happen due to moving parts or cables or other things that don't show up in the 3D mesh.
 * Since the 3D mesh is used for collision checking, those things will not be considered for collision which can lead to the robot colliding!
 * To work around this, we intreduced CPVs. They are simple volumina placed around the module that are considered in collision checking and
 * therefore block certain spaces for the robot. For Target s that have the linear_path tag set to true, all CPVs will be disabled as those Target s
 * usually lay at within the module.
 * CPVs can be set through rosparams defined in the cpv_params.yaml file.
 * 
 */
struct CPV{
    std::string id; ///< The identifier of the CPV. Starts with the associated modules id.
    CPVShapes shape; ///< The basic shape of the CPV.
    std::vector<float> dimensions; ///< Vector defining the size of the CPV in its different dimensions.
    geometry_msgs::Pose pose; ///< Pose of the CPV in the associated modules coordinate frame.
};

/**
 * @brief Modules are the elements that make up the robots Evironment.
 * 
 * All the machines and elements in the robots environment should be represented as modules.
 * All that is not, will not be considered for collision checking.
 * 
 */
class Module {
    friend class Environment;
    public:
        Module(std::string, float, float, int, bool);
        Module();
        ~Module();
        std::string getId();
        std::string getMeshURL();
        void setMeshURL(std::string);
        float getPosX();
        float getPosY();
        float getOrientation();
        void setPosX(float);
        void setPosY(float);
        void setOrientation(float);
        void addCPV(std::string, CPVShapes, geometry_msgs::Pose, double dim1=0, double dim2=0, double dim3=0);
        std::vector<CPV> getCPVs();
        void addKeyPoint(std::size_t, geometry_msgs::Pose);
        void clearKeyPoints();
        geometry_msgs::Pose getModulePose();
        bool hasPlate();
        shape_msgs::Mesh getMeshShapeMsg();
        geometry_msgs::Pose getKeyPoint(std::size_t);
    


    private:
        geometry_msgs::Pose module_pose; ///< The pose of the module in the planning coordinate frame (base_link)
        geometry_msgs::TransformStamped module_frame_transform; ///< Coordinate transform between the module coordinate frame and the planning coordinate frame (base_link)
        std::string id; ///< The modules identifier
        std::string mesh_url; ///< URL where the 3D mesh of the module can be found.
        bool has_plate; ///< Indicates whether or not the base is already included in the 3D mesh.
        float pos_x; ///< Position of the module on the x-axis.
        float pos_y; ///< Position of the momdule on the y-axis
        float orientation; ///< Orientation of the module around the z-axis.
        std::vector<CPV> cpvs; ///< Vector containing the CPV s associated with this module.
        std::map<std::size_t, geometry_msgs::Pose> key_points; ///< The key points associated with that module. key: numerical id, value: key point as pose.
};
#endif