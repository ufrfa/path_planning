#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <path_planning_ros_interface/PlanAction.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <nlohmann/json.hpp>
#include <nlohmann/valijson_nlohmann_bundled.hpp>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <moveit/moveit_cpp/moveit_cpp.h>
#include <moveit/moveit_cpp/planning_component.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit/transforms/transforms.h>
#include <moveit/robot_state/conversions.h>
#include <moveit_msgs/RobotState.h>
#include <moveit_msgs/RobotTrajectory.h>
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "tf2_ros/transform_listener.h"
#include <iostream>
#include <math.h>
#include <cmath>
#include "sole/sole.hpp"
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <Eigen/Geometry>
#include <handling_unit/environment.hpp>
#include <moveit/collision_detection/collision_common.h>
#include <boost/optional.hpp>
#include <handling_unit/planning_request.hpp>
#include <iterator>

/**
 * @brief The action server that processes planning requests and does the acutal path planning.
 * 
 */
class PlanActionServer
{
    private:
        moveit_cpp::MoveItCppPtr moveit_cpp_ptr = nullptr; ///< Pointer to the moveit_cpp object
        std::shared_ptr<moveit_cpp::PlanningComponent> planning_component; ///< Pointer to the planning component that does the planning. See <a href="http://docs.ros.org/en/noetic/api/moveit_ros_planning/html/classmoveit__cpp_1_1PlanningComponent.html">moveit_cpp::PlanningComponent</a>
        std::shared_ptr<std::vector<double>> start_joint_values; ///< Vector that holds the joint angle values of the robots start state.
        ros::NodeHandle* nh_ptr = nullptr; ///< Pointer to the path_planning node handle.
        std::string end_effector_link_name; ///< Name of the end effector link of the robot. For example tool0 or gripper_tcp
        const moveit::core::JointModelGroup* joint_model_group; ///< Pointer to the joint model group of the robot.
        std::shared_ptr<Environment> env; ///< Pointer to the robots Environment object
        bool success;
        std::string error_info;
        nlohmann::json trajectory_json; ///< The planned trajectory in json format.

        void setResult();
        void setUpObject(moveit::core::RobotStatePtr, bool);
        moveit_cpp::PlanningComponent::MultiPipelinePlanRequestParameters setUpMultiPipelinePlanRequest();
        void displayTrajectory(robot_trajectory::RobotTrajectoryPtr, moveit::core::RobotStatePtr);
        void createResultJSON(std::vector<robot_trajectory::RobotTrajectoryPtr>);
        void setAborted(std::string);
        std::vector<double> calculateJointValuesForPose(geometry_msgs::PoseStamped, boost::optional<std::size_t> = {});
        geometry_msgs::PoseStamped calculatePoseForJointValues(std::vector<double>, std::string = "base_link");
        nlohmann::json checkResult(nlohmann::json);

    protected:
        ros::NodeHandle nh_; ///< Node handle of the path_planning node. Needed to build the action server.
        actionlib::SimpleActionServer<path_planning_ros_interface::PlanAction> as_; ///< PlanAction object. NodeHandle instance must be created before. Otherwise strange error occurs.
        std::string action_name_; ///< Name of the action. Should be "plan_action_server"
        // create messages that are used to published feedback/result
        path_planning_ros_interface::PlanFeedback feedback_; ///< Action feedback object.
        path_planning_ros_interface::PlanResult result_; ///< Action result object.

        static std::vector<planning_interface::MotionPlanResponse> getSolutions(const std::vector<planning_interface::MotionPlanResponse>&);
        static double weightedDistance(robot_trajectory::RobotTrajectoryPtr trajectory);
        static planning_interface::MotionPlanResponse getShortestWeightedSolution(const std::vector<planning_interface::MotionPlanResponse>&);
        static bool stoppingCriterion(moveit_cpp::PlanningComponent::PlanSolutions const&, moveit_cpp::PlanningComponent::MultiPipelinePlanRequestParameters const&);
        static bool stoppingCriterionOneSolution(moveit_cpp::PlanningComponent::PlanSolutions const&, moveit_cpp::PlanningComponent::MultiPipelinePlanRequestParameters const&);

    public:
        PlanActionServer(std::string name, moveit_cpp::MoveItCppPtr, ros::NodeHandle*, std::shared_ptr<Environment>, std::shared_ptr<moveit_cpp::PlanningComponent>);
        ~PlanActionServer();
        void executeCB(const path_planning_ros_interface::PlanGoalConstPtr &goal);

};