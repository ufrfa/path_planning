#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <vector>
#include "handling_unit/module.hpp"
#include <string>
#include <map>
#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <moveit_msgs/CollisionObject.h>
#include "geometric_shapes/shapes.h"
#include "geometric_shapes/mesh_operations.h"
#include "geometric_shapes/shape_operations.h"
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/collision_detection/collision_common.h>
#include <boost/optional.hpp>

/**
 * @brief Struct defining a base plate.
 * 
 * The environment of the robot consits of Tables. Each Table consists of 4 BasePlate.
 * 
 */
struct BasePlate{
    std::string id; ///< Identifier of the BasePlate.
    float pos_x; ///< Position on the x-axis.
    float pos_y; ///< Position on the y-axis.
};

/**
 * @brief Struct defining a Table.
 *
 * The environment of the robot consists of Tables. Each Table consists of 4 BasePlate.
 * We can place one Module on each BasePlate.
 * 
 */
struct Table{
    std::string id; ///< Identifier of the Table.
    float pos_x; ///< Position on the x-axis.
    float pos_y; ///< Position on the y-axis.
    std::vector<BasePlate> base_plates; ///< Vector containing the BasePlate s associated with this Table.
};

/**
 * @brief The Environment of the Robot.
 *
 * This class is used to hold the virtual representation of the robots environment for collision checking.
 * All the necessery elements such as Table or Module can be managed here. 
 * This class also manages the different coordinate frames.
 * 
 */
class Environment {
    public:
        Environment(std::string, std::shared_ptr<moveit::planning_interface::PlanningSceneInterface>, planning_scene_monitor::PlanningSceneMonitorPtr);
        ~Environment();
        Table getTable(std::string);
        Module getModule(std::string);
        tf2_ros::Buffer* getTransformBuffer();
        geometry_msgs::TransformStamped getTransform(std::string, std::string);
        void addTable(Table);
        void addModule(Module);
        void buildEnvironment();
        void addRobotPosition(float, float);
        void reset();
        void removeTable(std::string);
        void removeModule(std::string);
        collision_detection::CollisionResult checkCollision(robot_state::RobotState, boost::optional<bool> = {}, boost::optional<bool> = {}, boost::optional<bool> = {}, boost::optional<std::string> = {});
        geometry_msgs::PoseStamped getKeyPoint(std::string, std::size_t);
        void deactivateCPVs();
        void activateCPVs();

    private:
        std::map<std::string, Table> tables; ///< The Table s in the robots environment. key: id, value: associated Table
        std::map<std::string, Module> modules; ///< The Module s in the robots environment. key: id, value: associated Module
        const tf2_ros::TransformListener* tf_listener_ptr; ///< Listener for the coordinate frame transformations.
        tf2_ros::Buffer* tf_buffer_ptr; ///< Buffer to store coordinate frame transformations.
        std::string frame_id; ///< The name of the planning coordinate frame (base_link).
        std::shared_ptr<moveit::planning_interface::PlanningSceneInterface> planning_scene_interface; ///< Pointer to interact with the robots planning scene.
        planning_scene_monitor::PlanningSceneMonitorPtr psm; ///< Pointer to the planning scene monitor to keep track of the robots planning scene.

        void buildTable(std::string);
        void buildModule(std::string);
        void buildBasePlate(BasePlate&);
        void buildCollisionProtectionVolumina(Module&);
        void setModulePose(std::string, std::string);
        void setModuleFrameTransform(std::string);
};

#endif
