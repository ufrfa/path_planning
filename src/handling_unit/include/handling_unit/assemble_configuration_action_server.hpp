#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <path_planning_ros_interface/Assemble_configurationAction.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <nlohmann/json.hpp>
#include <algorithm>
#include <string>
#include <tf2_ros/static_transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>
#include <handling_unit/environment.hpp>
#include <nlohmann/valijson_nlohmann_bundled.hpp>
#include <fstream>
#include <sstream>
#include <xmlrpcpp/XmlRpcValue.h>

/**
 * @brief The action server that builds the robots environment by assembling the received configuration of the station.
 * 
 */
class AssembleConfigurationActionServer
{
    private:
        std::shared_ptr<Environment> env; ///< Pointer to the robots Environment object
        
        void abortCB(std::string);
        bool buildTables(nlohmann::json);
        bool buildModules(nlohmann::json);
        void buildCPVs(std::string, Module &new_module);
        bool updateModule(nlohmann::json, std::string);
        bool addModule(nlohmann::json, std::string);
        bool addKeyPoints(Module &new_module, nlohmann::json);

    protected:
        ros::NodeHandle nh_; ///< Node handle of the path_planning node. Needed to build the action server.
        actionlib::SimpleActionServer<path_planning_ros_interface::Assemble_configurationAction> as_; ///< Assemble_configurationAction object. NodeHandle instance must be created before this line. Otherwise strange error occurs.
        std::string action_name_; ///< Name of the action. Should be "assemble_configuration_action_server"
        // create messages that are used to published feedback/result
        path_planning_ros_interface::Assemble_configurationFeedback feedback_;
        path_planning_ros_interface::Assemble_configurationResult result_;

    public:
        AssembleConfigurationActionServer(std::string name, std::shared_ptr<Environment>);

        ~AssembleConfigurationActionServer(void);

        void executeCB(const path_planning_ros_interface::Assemble_configurationGoalConstPtr &goal);
        void validateJson(nlohmann::json);

};