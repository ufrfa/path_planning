#ifndef PLANNING_REQUEST_H
#define PLANNING_REQUEST_H

#include <vector>
#include <nlohmann/json.hpp>
#include <nlohmann/valijson_nlohmann_bundled.hpp>
#include <fstream>
#include <sstream>
#include <handling_unit/environment.hpp>
#include <string>
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "tf2_ros/transform_listener.h"

/**
 * @brief Types of Target s that can be processed.
 * 
 * GLOBAL_POSE_TARGET: A Target defined as as pose in the global coordinate frame.
 * MODULE_POSE_TARGET: A Target defined as a pose in a modules coordinate frame. Those poses can also be key points of the Module.
 * STATE_TARGET: A Target defined as a vector of joint angles.
 * 
 */
enum TargetTypes{
    GLOBAL_POSE_TARGET,
    MODULE_POSE_TARGET,
    STATE_TARGET
};

/**
 * @brief The struct defining Target objects.
 * 
 */
struct Target {
    private:
        double gripper_state; ///< The joint angle of the gripper.
        bool object_gripped; ///< Indicates whether or not a gripped object has to be considered for collision checking.
        std::vector<double> joint_values; ///< Joint angles for STATE_TARGET
        geometry_msgs::PoseStamped pose; ///< The target pose.
        /**
         * @brief Indicates a Target that constitutes an interaction with a Module.
         * 
         * Those Target s typically lay very close to the Module or inside them and therefore require special treatment. Right now a Target
         * that has this attribute set to true will deactivate alle CPV s during collision checking. Originally we also wanted to include
         * a line constraint for movements to those Target s. This attribute is only used for MODULE_POSE_TARGET.
         * 
         */
        bool linear_path;
        TargetTypes type; ///< The type of this target

    public:
        /**
         * @brief Construct a new Target object
         * 
         * @param type The type of this target
         */
        Target(TargetTypes type){this->type = type;};

        /**
         * @brief Destroy the Target object
         * 
         */
        ~Target(){};
        
        /**
         * @brief Get the type of the target
         * 
         * @return TargetTypes 
         */
        TargetTypes getType(){return this->type;};

        /**
         * @brief Set the angle of the gripper joint.
         * 
         * @param gripper_state 
         */
        void setGripperState(double gripper_state){ this->gripper_state = gripper_state;};

        /**
         * @brief Add or remove a gripped object from the robot for collision checking.
         * 
         * @param object_gripped true: add object, false: remove object
         */
        void setObjectGripped(bool object_gripped){ this->object_gripped = object_gripped;};

        /**
         * @brief Get the angle of the gripper joint.
         * 
         * @return double 
         */
        double getGripperState(){ return this->gripper_state;};

        /**
         * @brief Return whether or not an object is gripped in the robots collision model.
         * 
         * @return true
         * @return false 
         */
        bool isObjectGripped(){ return this->object_gripped;};

        /**
         * @brief Return the joint values of a STATE_TARGET. Only if Target is of type STATE_TARGET.
         * 
         * @return std::vector<double> Vector with joint values.
         */
        std::vector<double> getJointValues(){
            if(this->type == TargetTypes::STATE_TARGET && this->joint_values.size() > 0)
                return this->joint_values;
            else
                throw std::runtime_error("Can't access joint values. Target is not a State Target.");
        };

        /**
         * @brief Get the target pose. Only if Target is of tye GLOBAL_POSE_TARGET or MODULE_POSE_TARGET.
         * 
         * @return geometry_msgs::PoseStamped The target pose. Should always be in the base_link coordinate frame.
         */
        geometry_msgs::PoseStamped getPose(){
            if(this->type == TargetTypes::GLOBAL_POSE_TARGET || this->type == TargetTypes::MODULE_POSE_TARGET)
                return this->pose;
            else
                throw std::runtime_error("Can't access pose. Target is not Global Pose Target or Module Pose Target.");
        };

        /**
         * @brief Indicates whether this Target that constitutes an interaction with a Module.
         * 
         * @return true Target constitutes an interaction with a Module.
         * @return false Target does not constitute an interaction with a Module.
         */
        bool isLinearPath(){
            if(this->type == TargetTypes::MODULE_POSE_TARGET)
                return this->linear_path;
            else
                throw std::runtime_error("Can't acces whether target requieres linear path. Target is not Module Pose Target.");
        };

        /**
         * @brief Set the Joint Values object
         * 
         * @param joint_values 
         */
        void setJointValues(std::vector<double> joint_values){
            if(this->type == TargetTypes::STATE_TARGET)
                this->joint_values = joint_values;
        }

        /**
         * @brief Set the target pose. Only GLOBAL_POSE_TARGET or MODULE_POSE_TARGET.
         * 
         * @param pose Pose should always be in the base_link coordinate frame.
         */
        void setPose(geometry_msgs::PoseStamped pose){
            if (this->type == TargetTypes::GLOBAL_POSE_TARGET || this->type == TargetTypes::MODULE_POSE_TARGET)
                this->pose = pose;
        };

        /**
         * @brief Set whether or not this Target constitues an interaction with a Module.
         * 
         * @param linear_path 
         */
        void setLinearPath(bool linear_path){
            if(this->type == TargetTypes::MODULE_POSE_TARGET)
                this->linear_path = linear_path;
        };
};

/**
 * @brief A PlanningRequest holds and manages a group of consecutive and connected Target s.
 * 
 */
class PlanningRequest {
    public:
        PlanningRequest(std::shared_ptr<Environment>);
        ~PlanningRequest();
        void initPlanningRequest(nlohmann::json);
        std::vector<std::shared_ptr<Target>> getTargets();
        std::vector<double> getStartJointValues();

    private:
        std::vector<std::shared_ptr<Target>> targets; ///< Vector with pointers to the Target s in this PlanningRequest. All pose targets here should be in the base_link coordinate frame.
        std::vector<double> start_joint_values; ///< Vector containing the joint angles that constitute the start state for this PlanningRequest
        std::shared_ptr<Environment> env; ///< Pointer to the robots Environment
        nlohmann::json planning_request_json_schema; ///< A json schema to ensure received planning requests in json format can be translated into a PlanningRequest object.

        void setStartJointValues(nlohmann::json);
        std::vector<double> getJointValues(nlohmann::json);
        void addTarget(nlohmann::json);
        geometry_msgs::Pose getTargetPose(nlohmann::json);
        void addStateTarget(nlohmann::json);
        void addGlobalPoseTarget(nlohmann::json);
        void addModulePoseTarget(nlohmann::json);
        double extractGripperState(nlohmann::json);
        bool extractObjectGripped(nlohmann::json);
};
#endif