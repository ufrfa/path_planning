#include "handling_unit/plan_action_server.hpp"

/**
 * @brief Construct a new Plan Action Server:: Plan Action Server object
 * 
 * @param name Name of the action. Should be "plan_action_server".
 * @param moveit_cpp_ptr Pointer to the moveit cpp object.
 * @param nh_ptr Pointer to the NodeHandle of the path_planning node.
 * @param env Pointer to the robots Environment object.
 * @param planning_component Pointer to the planning component that does the planning. See <a href="http://docs.ros.org/en/noetic/api/moveit_ros_planning/html/classmoveit__cpp_1_1PlanningComponent.html">moveit_cpp::PlanningComponent</a>
 */
PlanActionServer::PlanActionServer(std::string name, moveit_cpp::MoveItCppPtr moveit_cpp_ptr, ros::NodeHandle* nh_ptr, std::shared_ptr<Environment> env, std::shared_ptr<moveit_cpp::PlanningComponent> planning_component) :
            as_(nh_, name, boost::bind(&PlanActionServer::executeCB, this, _1), false),
            action_name_(name){
                as_.start();
                this->moveit_cpp_ptr = moveit_cpp_ptr;
                this->start_joint_values = std::make_shared<std::vector<double>>();
                this->nh_ptr = nh_ptr;
                this->joint_model_group = this->moveit_cpp_ptr->getRobotModel()->getJointModelGroup("arm");
                this->env = env;
                this->planning_component = planning_component;
                this->end_effector_link_name = "gripper_tcp"; //gripper_tcp tool0
                //this->solutions_ptr = nullptr;
                ROS_INFO("Started Planning Request Action Server");
            }

/**
 * @brief Destroy the Plan Action Server:: Plan Action Server object
 * 
 */
PlanActionServer::~PlanActionServer(void){}

/**
 * @brief Calculate the total distance covered by a trajectory in the joint space with weights applied to certain joints.
 *
 * Tajectories with a shorter total distance in joint space indicate more efficient movement.
 * The weights are read from rosparams defined in planning_params.yaml
 * 
 * @param trajectory Pointer to the trajectory object
 * @return double Total weighted distance in joint space.
 */
double PlanActionServer::weightedDistance(robot_trajectory::RobotTrajectoryPtr trajectory){
    double distance = 0.0;
    std::vector<double> joint_values_1;
    std::vector<double> joint_values_2;
    std::vector<double> weights;
    std::map<std::string, double> joint_weights;
    if(!ros::param::get("path_planning/joint_weights", joint_weights)){
        ROS_WARN("COULD NOT GET JOINT WEIGHT PARAMS, USING DEFAULT VALUES.");
        weights.clear();
        weights.push_back(1.0);
        weights.push_back(1.0);
        weights.push_back(1.0);
        weights.push_back(1.0); //2.0
        weights.push_back(1.0); //2.0
        weights.push_back(1.0); //3.0
    } else {
        weights.clear();
        weights.push_back(joint_weights["shoulder_pan_joint"]);
        weights.push_back(joint_weights["shoulder_lift_joint"]);
        weights.push_back(joint_weights["elbow_joint"]);
        weights.push_back(joint_weights["wrist_1_joint"]);
        weights.push_back(joint_weights["wrist_2_joint"]);
        weights.push_back(joint_weights["wrist_3_joint"]);
    }
    for (std::size_t k = 1; k < trajectory->getWayPointCount(); ++k)
    {
        joint_values_1.clear();
        joint_values_2.clear();
        trajectory->getWayPoint(k-1).copyJointGroupPositions("arm", joint_values_1);
        trajectory->getWayPoint(k).copyJointGroupPositions("arm", joint_values_2);
        double point_distance = 0.0;
        for(std::size_t i = 0; i < joint_values_1.size(); ++i){
            point_distance += weights[i]*pow((joint_values_2[i] - joint_values_1[i]), 2.0);
        }
        distance += point_distance;
    }
    distance = sqrt(distance);
    ROS_INFO_NAMED("tutorial", "Distance: %lf", distance);
    return distance;  
}

/**
 * @brief Function that returns all found solutions.
 * 
 * When planning, we have to pass a function that selects solutions to the planning function.
 * This function here essentially returns its input and can therefore be used to just return all found solutions.
 * 
 * @param solutions All successfull solutions
 * @return std::vector<planning_interface::MotionPlanResponse> 
 */
std::vector<planning_interface::MotionPlanResponse> PlanActionServer::getSolutions(const std::vector<planning_interface::MotionPlanResponse>& solutions){
    std::vector<planning_interface::MotionPlanResponse> successfull_mprs;
    if(solutions.empty()){
        return successfull_mprs;
    }
    for(planning_interface::MotionPlanResponse mpr : solutions){
        successfull_mprs.push_back(mpr);
    }
    return successfull_mprs;
}

/**
 * @brief Select and return the solution with the shortest weighted distance in joint space from all the found solutions.
 * 
 * @param solutions All found solutions.
 * @return planning_interface::MotionPlanResponse Solution with the shortest weighted distance in joint space.
 */
planning_interface::MotionPlanResponse PlanActionServer::getShortestWeightedSolution(const std::vector<planning_interface::MotionPlanResponse>& solutions){
    // Find trajectory with minimal path
    if(solutions.empty()){
        planning_interface::MotionPlanResponse mpr;
        mpr.error_code_ = moveit::core::MoveItErrorCode::FAILURE;
        return mpr;
    }

    auto const shortest_solution = std::min_element(solutions.begin(), solutions.end(),
        [](const planning_interface::MotionPlanResponse& solution_a,
        const planning_interface::MotionPlanResponse& solution_b) {
            // If both solutions were successful, check which path is shorter
            if (solution_a && solution_b)
            {
                //robot_trajectory::RobotTrajectoryPtr robot_traj = mpr.trajectory_;
                    // compute path length
                return weightedDistance(solution_a.trajectory_) < weightedDistance(solution_b.trajectory_);
            }
            // If only solution a is successful, return a
            else if (solution_a)
            {
                return true;
            }
            // Else return solution b, either because it is successful or not
            return false;
        });
    return *shortest_solution;
}

/**
 * @brief Stopping cirterion for the planning algorithm that uses all the planning time available.
 * 
 * @param plan_solutions 
 * @param plan_request_parameters 
 * @return true Always returns false.
 * @return false Always returns false.
 */
bool PlanActionServer::stoppingCriterion(
    moveit_cpp::PlanningComponent::PlanSolutions const& plan_solutions,
    moveit_cpp::PlanningComponent::MultiPipelinePlanRequestParameters const& plan_request_parameters)
{
  // Read solutions that are found up to this point from a thread safe storage
  //auto const& solutions = plan_solutions.getSolutions();
  //if(!solutions.empty()){
    //Stop if any solution is found
    //return true;
  //}
  
  // use all the planning time available
  return false;
}

/**
 * @brief Stopping criterion for the planning algorithm that stops as soon as a solution is found.
 * 
 * @param plan_solutions 
 * @param plan_request_parameters 
 * @return true Any solution is found.
 * @return false No solution found.
 */
bool PlanActionServer::stoppingCriterionOneSolution(
    moveit_cpp::PlanningComponent::PlanSolutions const& plan_solutions,
    moveit_cpp::PlanningComponent::MultiPipelinePlanRequestParameters const& plan_request_parameters)
{
  // Read solutions that are found up to this point from a thread safe storage
  auto const& solutions = plan_solutions.getSolutions();
  if(!solutions.empty()){
    //Stop if any solution is found
    return true;
  }
  // use all the planning time available
  return false;
}

/**
 * @brief Set the action result from the success and error_info attributes.
 * 
 */
void PlanActionServer::setResult(){
    nlohmann::json result_json;
    result_json["success"] = this->success;
    result_json["error_info"] = this->error_info;
    this->result_.result = this->checkResult(result_json).dump(4);
}

/**
 * @brief Set the action result to aborted.
 * 
 * @param error_message The reason why the action was aborted.
 */
void PlanActionServer::setAborted(std::string error_message){
    this->success = false;
    this->error_info = error_message;
    setResult();
    if (this->as_.isActive())
        this->as_.setAborted(this->result_);
    return;
}

/**
 * @brief Check the result json object against the schema defined in planning_request_result_schema.json
 * 
 * @param result The result json object
 * @return nlohmann::json The checked result json object. If the check was not successfull, error message is written into error_info field of the result json.
 */
nlohmann::json PlanActionServer::checkResult(nlohmann::json result){
    try{
        std::ifstream file("../../../src/handling_unit/json_interface/planning_request_result_schema.json");
        std::stringstream string_buffer;
        string_buffer << file.rdbuf();
        file.close();
        nlohmann::json json_schema = nlohmann::json::parse(string_buffer.str());
        valijson::Schema schema;
        valijson::SchemaParser parser;
        valijson::adapters::NlohmannJsonAdapter schema_document_adapter(json_schema);
        try{
            parser.populateSchema(schema_document_adapter, schema);
        } catch(std::exception &e) {
            throw std::runtime_error("Could not parse the validation schema. " + (std::string)e.what());
        }
        valijson::Validator validator;
        valijson::adapters::NlohmannJsonAdapter target_document_adapter(result);
        valijson::ValidationResults validation_results;
        if(!validator.validate(schema, target_document_adapter, &validation_results)){
            valijson::ValidationResults::Error error;
            std::string error_string = "Validation of schema failed for planning request, here is why: \n";
            while(validation_results.popError(error)){
                std::string context;
                std::vector<std::string>::iterator itr = error.context.begin();
                for (; itr != error.context.end(); itr++) {
                    context += *itr;
                }
                error_string += ("context: " + context + "\ndescription: " + error.description + "\n");
            }
            throw std::runtime_error(error_string);
        }
    } catch(std::exception &e){
        nlohmann::json result_json;
        result_json["success"] = false;
        std::string validation_error_info = std::string(e.what());
        if(result.contains("error_info") && result["error_info"] != "{}"){
            result_json["error_info"] = std::string(result["error_info"]) + "\n ";
        }
        result_json["error_info"] += validation_error_info;
        return result_json;
    }
    return result;
}

/**
 * @brief Add or remove a rectangular object to the robots gripper for collision checking.
 * 
 * @param robot_start_state Pointer to the start state for this planning step.
 * @param object_gripped true: add object, false: remove object
 */
void PlanActionServer::setUpObject(moveit::core::RobotStatePtr robot_start_state, bool object_gripped){
    if(object_gripped && !robot_start_state->hasAttachedBody("poltopf_long")){
        const std::vector<std::string> touch_links = {"left_inner_finger_pad", "right_inner_finger_pad"};
        Eigen::Transform<double, 3, Eigen::Isometry> t;
        t = Eigen::AngleAxis<double>(0.0, Eigen::Vector3d::UnitZ());
        t.rotate(Eigen::AngleAxis<double>(0.0, Eigen::Vector3d::UnitX()));
        t.rotate(Eigen::AngleAxis<double>(0.0, Eigen::Vector3d::UnitY()));
        t.translate(Eigen::Vector3d(0.02, 0.0, 0.0));
        Eigen::Isometry3d pose;
        pose.translation() = t.translation();
        pose.linear() = t.rotation();
        t.setIdentity();
        Eigen::Isometry3d shape_pose;
        shape_pose.translation() = t.translation();
        shape_pose.linear() = t.rotation();
        //shapes::Box object(0.09, 0.03, 0.05);
        shapes::ShapeConstPtr object = std::make_shared<shapes::Box>(0.09, 0.03, 0.05);
        const std::vector<shapes::ShapeConstPtr> shapes = {object};
        EigenSTL::vector_Isometry3d shape_poses = {shape_pose}; 
        robot_start_state->attachBody(
            "poltopf_long",
            pose,
            shapes,
            shape_poses,
            touch_links,
            "gripper_tcp"
	    );
    } else if(!object_gripped && robot_start_state->hasAttachedBody("poltopf_long")){
        robot_start_state->clearAttachedBody("poltopf_long");
    }
}

/**
 * @brief Perform inverse kinematics on a target pose.
 * 
 * @throws runtime_error Inverse kinematics could not be solved.
 * @param target_pose Target pose.
 * @param max_attempts_after_timeout Max attempts for the inverse kinematics solver. Default 200.
 * @return std::vector<double> Vector with joint angle values.
 */
std::vector<double> PlanActionServer::calculateJointValuesForPose(geometry_msgs::PoseStamped target_pose, boost::optional<std::size_t> max_attempts_after_timeout){
    std::size_t max_attempts_after_timeout_default = 200;
    if(target_pose.header.frame_id != "base_link"){
        this->env->getTransformBuffer()->transform(target_pose, target_pose, "base_link");
    }
    moveit::core::RobotStatePtr seed_state = this->moveit_cpp_ptr->getCurrentState();
    seed_state->setToRandomPositions();
    std::vector<double> seed_state_joint_values;
    seed_state->setToRandomPositions();
    seed_state->copyJointGroupPositions(this->planning_component->getPlanningGroupName(), seed_state_joint_values);
    //seed_state->setJointGroupPositions(this->planning_component->getPlanningGroupName(), seed_state_joint_values_);
    std::vector<double> solution;
    moveit_msgs::MoveItErrorCodes error_code;
    this->moveit_cpp_ptr->getRobotModel()->getJointModelGroup(this->planning_component->getPlanningGroupName())->getSolverInstance()->getPositionIK(target_pose.pose, seed_state_joint_values, solution, error_code);
    if(error_code.val == moveit_msgs::MoveItErrorCodes::TIMED_OUT){
        std::size_t attempts = 0;
        while(error_code.val == moveit_msgs::MoveItErrorCodes::TIMED_OUT && attempts < max_attempts_after_timeout.value_or(max_attempts_after_timeout_default)){
            seed_state->setToRandomPositions();
            std::vector<double> seed_state_joint_values;
            seed_state->copyJointGroupPositions(this->planning_component->getPlanningGroupName(), seed_state_joint_values);
            this->moveit_cpp_ptr->getRobotModel()->getJointModelGroup(this->planning_component->getPlanningGroupName())->getSolverInstance()->getPositionIK(target_pose.pose, seed_state_joint_values, solution, error_code);
            attempts++;
        }
    }
    if(error_code.val != moveit_msgs::MoveItErrorCodes::SUCCESS)
            throw std::runtime_error(std::to_string((int)error_code.val));
    return solution;
}

/**
 * @brief Calculate the forward kinematics for a vector of joint angle values
 * 
 * @param joint_values Vector of joint angle values.
 * @param frame_id The desired coordinate frame for the pose.
 * @return geometry_msgs::PoseStamped Pose calculated from joint angle values.
 */
geometry_msgs::PoseStamped PlanActionServer::calculatePoseForJointValues(std::vector<double> joint_values, std::string frame_id){
    std::vector<std::string> link_names;
    link_names.push_back(this->end_effector_link_name);
    std::vector<geometry_msgs::Pose> poses;
    if(!this->moveit_cpp_ptr->getRobotModel()->getJointModelGroup(this->planning_component->getPlanningGroupName())->getSolverInstance()->getPositionFK(link_names, joint_values, poses)){
        throw std::runtime_error("");
    }
    geometry_msgs::PoseStamped pose;
    pose.pose = poses[0];
    pose.header.frame_id = frame_id;
    if(frame_id == "base_link")
        return pose;
    this->env->getTransformBuffer()->transform(pose, pose, frame_id);
    return pose;
}

/**
 * @brief Set up the planning request parameters with rosparams from planning_params.yaml
 * 
 * @return moveit_cpp::PlanningComponent::MultiPipelinePlanRequestParameters
 */
moveit_cpp::PlanningComponent::MultiPipelinePlanRequestParameters PlanActionServer::setUpMultiPipelinePlanRequest(){
    moveit_cpp::PlanningComponent::MultiPipelinePlanRequestParameters multi_pipeline_plan_request{*(this->nh_ptr), {}};
    double planning_time;
    int planning_attempts;
    double max_velocity_scaling_factor;
    double max_acceleration_scaling_factor;
    std::vector<std::string> planners;
    std::map<std::string, std::string> pipelines;
    if(!this->nh_ptr->getParam("planning_time", planning_time)
        || !this->nh_ptr->getParam("planning_attempts", planning_attempts)
        || !this->nh_ptr->getParam("max_velocity_scaling_factor", max_velocity_scaling_factor)
        || !this->nh_ptr->getParam("max_acceleration_scaling_factor", max_acceleration_scaling_factor)
        || !this->nh_ptr->getParam("planners", planners)
        || !this->nh_ptr->getParam("pipelines", pipelines)){
        ROS_WARN("COULD NOT LOAD PLANNING PARAMS, USING DEFAULT PARAMS INSTEAD!");
        planning_time = 5.0;
        planning_attempts = 5;
        max_velocity_scaling_factor = 1.0;
        max_acceleration_scaling_factor = 1.0;
        planners.clear();
        planners.push_back("RRTstar");
        planners.push_back("BiTRRT");
        planners.push_back("PRMstar");
        pipelines.clear();
        pipelines["RRTstar"] = "ompl_rrt_star";
        pipelines["BiTRRT"] = "ompl_bitrrt";
        pipelines["PRMstar"] = "ompl_prm_star";
    }
    multi_pipeline_plan_request.multi_plan_request_parameters.clear();
    multi_pipeline_plan_request.multi_plan_request_parameters.reserve(planners.size());
    for(std::string planner : planners){
        std::string pipeline = pipelines[planner];
        moveit_cpp::PlanningComponent::PlanRequestParameters params;
        params.planning_attempts = planning_attempts;
        params.max_velocity_scaling_factor = max_velocity_scaling_factor;
        params.max_acceleration_scaling_factor = max_acceleration_scaling_factor;
        params.planning_time = planning_time;
        params.planner_id = planner;
        params.planning_pipeline = pipeline;
        multi_pipeline_plan_request.multi_plan_request_parameters.push_back(params);
        }
    return multi_pipeline_plan_request;
}

/**
 * @brief Create the result json from the solution trajectories.
 * 
 * @param trajectories Vector of pointer to the found solution trajectories.
 */
void PlanActionServer::createResultJSON(std::vector<robot_trajectory::RobotTrajectoryPtr> trajectories){
    sole::uuid traj_id = sole::uuid1();
    this->trajectory_json["id"] = traj_id.str();
    this->trajectory_json["success"] = true;
    for(robot_trajectory::RobotTrajectoryPtr trajectory : trajectories){
        nlohmann::json sub_trajectory_json;
        std::vector<double> joint_positions;
        for(std::size_t i = 0; i < trajectory->getWayPointCount(); ++i){
            joint_positions.clear();
            trajectory->getWayPoint(i).copyJointGroupPositions(this->joint_model_group, joint_positions);
            nlohmann::json waypoint_json;
            for(double joint_value : joint_positions){
                waypoint_json["positions"] += joint_value;
            }
            sub_trajectory_json["waypoints"] += waypoint_json;
        }
        this->trajectory_json["trajectories"] += sub_trajectory_json;
    }

}

/**
 * @brief Display a trajectory in RViz
 * 
 * @param robot_traj Pointer to the trajectory.
 * @param robot_start_state Pointer to the robots start state.
 */
void PlanActionServer::displayTrajectory(robot_trajectory::RobotTrajectoryPtr robot_traj, moveit::core::RobotStatePtr robot_start_state){
    ros::Publisher display_publisher = this->nh_ptr->advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1, true);
    moveit_msgs::DisplayTrajectory display_trajectory;
    moveit_msgs::RobotTrajectory robot_traj_msg;
    moveit_msgs::RobotState robot_state_msg;
    robot_traj->getRobotTrajectoryMsg(robot_traj_msg);
    moveit::core::robotStateToRobotStateMsg (*robot_start_state, robot_state_msg, true);
    display_trajectory.trajectory_start = robot_state_msg;
    display_trajectory.trajectory.push_back(robot_traj_msg);
    sleep(2.0);
    display_publisher.publish(display_trajectory);
    sleep(1.0);
}

/**
 * @brief The action callback function.
 * 
 * This function sets up the robot for the planning problem and then iteratively reads the received targets and performs the planning to those targets.
 * Finally, the result is sent back.
 * 
 * @param goal The received action goal.
 */
void PlanActionServer::executeCB(const path_planning_ros_interface::PlanGoalConstPtr &goal){
    this->success = false;
    this->error_info = "";
    //this->result_json = nlohmann::json();
    this->trajectory_json = nlohmann::json();
    if(as_.isPreemptRequested() || !ros::ok()){
        ROS_INFO("%s: Preempted", action_name_.c_str());
        this->success = false;
        this->error_info = "Action Preempted!";
        setResult();
        this->as_.setPreempted(result_);
        return;
    }
    this->feedback_.feedback = "Starting Planning.";
    this->as_.publishFeedback(this->feedback_);
    if(this->moveit_cpp_ptr == nullptr || this->nh_ptr == nullptr){
        this->setAborted("ERROR: No move group interface specified.");
        return;
    }
    std::string goal_msg = goal->planning_request;
    nlohmann::json goal_json = nlohmann::json::parse(goal_msg);
    PlanningRequest planning_request = PlanningRequest(this->env);
    try{
        planning_request.initPlanningRequest(goal_json);
    } catch(std::exception &ex){
        setAborted("Error while trying to read targets. " + (std::string)ex.what());
        return;
    }
    moveit::core::RobotStatePtr robot_start_state(new moveit::core::RobotState(*(this->moveit_cpp_ptr->getCurrentState())));
    if(planning_request.getStartJointValues().size() > 0){
        this->start_joint_values->clear();
        for(double value : planning_request.getStartJointValues()){
            this->start_joint_values->push_back(value);
            ROS_INFO("SET START JOINT VALUE TO: %lf", value);
        }
    } else if(this->start_joint_values->empty()){
        this->setAborted("ERROR: No start joint values specified");
        return;
    }
    robot_start_state->setJointGroupPositions(this->joint_model_group, *(this->start_joint_values));
    std::vector<std::shared_ptr<Target>> targets = planning_request.getTargets();
    moveit_cpp::PlanningComponent::MultiPipelinePlanRequestParameters multi_pipeline_plan_request = this->setUpMultiPipelinePlanRequest();
    planning_interface::MotionPlanResponse mpr;
    std::vector<robot_trajectory::RobotTrajectoryPtr> trajectories;
    moveit::core::RobotStatePtr new_start_state(new moveit::core::RobotState(*robot_start_state));
    for(std::vector<std::shared_ptr<Target>>::iterator target_itr = targets.begin();
        target_itr != targets.end();
        ++target_itr){
            ROS_INFO("PROCESS TARGET NUMBER %i", (std::distance(targets.begin(), target_itr)+1));
            mpr.error_code_ = moveit::core::MoveItErrorCode::FAILURE;
            const double gripper_state_value = (*target_itr)->getGripperState();
            new_start_state->setJointPositions(this->moveit_cpp_ptr->getRobotModel()->getJointModelGroup("gripper")->getJointModel("finger_joint"), &gripper_state_value);
            this->setUpObject(new_start_state, (*target_itr)->isObjectGripped());
            ROS_INFO("OBJECT ATTACHED? %i", new_start_state->hasAttachedBody("poltopf_long"));
            this->planning_component->setStartState(*new_start_state);
            if((*target_itr)->getType() == TargetTypes::GLOBAL_POSE_TARGET){
                if((*target_itr)->getPose().header.frame_id == ""){
                    setAborted("Error while trying to set Global Pose Target.");
                    return;
                }
                this->planning_component->setGoal((*target_itr)->getPose(), this->end_effector_link_name); //gripper_tcp
                ROS_INFO("GOT GLOBAL POSE TARGET");
                mpr = this->planning_component->plan(multi_pipeline_plan_request, &getShortestWeightedSolution, &stoppingCriterion); //, &stoppingCriterion
            } else if((*target_itr)->getType() == TargetTypes::STATE_TARGET){
                moveit::core::RobotStatePtr robot_state = this->moveit_cpp_ptr->getCurrentState();
                robot_state->setJointGroupPositions(this->joint_model_group, (*target_itr)->getJointValues());
                this->planning_component->setGoal(*robot_state);
                ROS_INFO("GOT STATE TARGET");
                mpr = this->planning_component->plan(multi_pipeline_plan_request, &getShortestWeightedSolution, &stoppingCriterion); //, &stoppingCriterion
            } else if((*target_itr)->getType() == TargetTypes::MODULE_POSE_TARGET){
                if(!(*target_itr)->isLinearPath()){
                    if((*target_itr)->getPose().header.frame_id == ""){
                        setAborted("Error while trying to set Global Pose Target.");
                        return;
                    }
                    this->planning_component->setGoal((*target_itr)->getPose(), this->end_effector_link_name);
                    ROS_INFO("GOT MODULE POSE TARGET");
                    mpr = this->planning_component->plan(multi_pipeline_plan_request, &getShortestWeightedSolution, &stoppingCriterion); //, &stoppingCriterion
                } else{
                    ROS_INFO("GOT SINGLE LINE TARGET");
                    this->env->deactivateCPVs();
                    ros::Duration(2).sleep();
                    //mpr = this->planLineTarget(*target_itr);
                    this->planning_component->setGoal((*target_itr)->getPose(), this->end_effector_link_name);
                    mpr = this->planning_component->plan(multi_pipeline_plan_request, &getShortestWeightedSolution, &stoppingCriterion); //, &stoppingCriterion
                    this->env->activateCPVs();
                    ros::Duration(2).sleep();
                }
            }
            if(mpr.error_code_ == moveit::core::MoveItErrorCode::SUCCESS){
                trajectories.push_back(mpr.trajectory_);
                this->displayTrajectory(mpr.trajectory_, new_start_state);
                const robot_state::RobotState& target_state = mpr.trajectory_->getWayPoint(mpr.trajectory_->getWayPointCount() - 1);
                this->start_joint_values->clear();
                target_state.copyJointGroupPositions(joint_model_group, *(this->start_joint_values));
                new_start_state->setJointGroupPositions(this->joint_model_group, *(this->start_joint_values));
            } else {
                this->setAborted("Could not find a feasible path for target number " + std::to_string((int)(std::distance(targets.begin(), target_itr)+1)));
                return;
            }
    }
    if(trajectories.empty()){
        this->setAborted("Could not find any feasible path.");
        return;
    }
    this->createResultJSON(trajectories);
    robot_trajectory::RobotTrajectoryPtr robot_traj = std::make_shared<robot_trajectory::RobotTrajectory>(this->moveit_cpp_ptr->getRobotModel(), this->planning_component->getPlanningGroupName());
    for(robot_trajectory::RobotTrajectoryPtr trajectory : trajectories){
        robot_traj->append(*trajectory, trajectory->getAverageSegmentDuration());
    }
    this->displayTrajectory(robot_traj, robot_start_state);
    this->result_.result = this->checkResult(this->trajectory_json).dump(4);
    if(this->as_.isActive())
        this->as_.setSucceeded(this->result_);
    return;
}
