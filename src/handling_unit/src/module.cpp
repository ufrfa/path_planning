#include "handling_unit/module.hpp"


/** 
 * @brief Constructor for Module class.
 * 
 * @param id Module id. This will also be the id of the modules own coordinate frame and thus must be unique in the system.
 * @param pos_x Position on the global x-axis.
 * @param pos_y Position on the global y-axis.
 * @param orientation Rotation around the global z-axis. This is discretizes as value in {0, 1, 2, 3}.
 * @param has_plate Indicates whether or not the module already includes a base_plate.
*/
Module::Module(std::string id, float pos_x, float pos_y, int orientation, bool has_plate){
    this->id = id;
    if(orientation != 0 && orientation != 1 && orientation != 2 &&orientation !=3 ){
        throw std::invalid_argument("Orientation must be in {0, 1, 2, 3}");
    }
    this->orientation = orientation;
    this->pos_x = pos_x;
    this->pos_y = pos_y;
    this->has_plate = has_plate;
    this->mesh_url = "";
}

/**
 * @brief Empty Constructor
 * 
 */
Module::Module(){}

/**
 * @brief Destroy the Module:: Module object
 * 
 */
Module::~Module(){
}

/**
 * @brief Returns the identifier of the module.
 * 
 * @return std::string
 */
std::string Module::getId(){
    return this->id;
}

/**
 * @brief Returns the URL from which the 3D-Mesh of the module is retrieved.
 * 
 * @return std::string 
 */
std::string Module::getMeshURL(){
    return this->mesh_url;
}

/**
 * @brief Set the URL from which the 3D-Mesh of the module is retrieved.
 * 
 * @param mesh_url The new URL.
 */
void Module::setMeshURL(std::string mesh_url){
    this->mesh_url = mesh_url;
}

/**
 * @brief Add a key point to the module.
 *
 * Key points are stored within a the module object an can be referenced by a numerical id.
 * 
 * @param id Numerical id to reference the key point. Must be an integer.
 * @param pose Key point as pose in the modules coordinate frame.
 */
void Module::addKeyPoint(std::size_t id, geometry_msgs::Pose pose){
    ROS_INFO("ADD KEY POINT %i", (int)id);
    ROS_INFO("position x: %lf", (double)pose.position.x);
    ROS_INFO("position y: %lf", (double)pose.position.y);
    ROS_INFO("position z: %lf", (double)pose.position.z);
    ROS_INFO("orientation x: %lf", (double)pose.orientation.x);
    ROS_INFO("orientation y: %lf", (double)pose.orientation.y);
    ROS_INFO("orientation z: %lf", (double)pose.orientation.z);
    ROS_INFO("orientation w: %lf", (double)pose.orientation.w);
    this->key_points[id] = pose;
}

/**
 * @brief Delete all stored key points.
 * 
 */
void Module::clearKeyPoints(){
    this->key_points.clear();
}

/**
 * @brief Returns the pose of the module in the planning coordinate frame (base_link).
 * 
 * @return geometry_msgs::Pose 
 */
geometry_msgs::Pose Module::getModulePose(){
    return this->module_pose;
}

/**
 * @brief Returns the position of the module on the x-axis.
 * 
 * This refers to the position in the discretized grid of the stations table.
 * 
 * @return float 
 */
float Module::getPosX(){
    return this->pos_x;
}

/**
 * @brief Returns the position of the module on the y-axis.
 * 
 * This refers to the position in the discretized grid of the stations table.
 * 
 * @return float 
 */
float Module::getPosY(){
    return this->pos_y;
}

/**
 * @brief Returns the orientation of the module.
 *
 * Orientation is discretized in steps of 90° around the z-axis, described by number 0, 1, 2, 3.
 * 
 * @return float 
 */
float Module::getOrientation(){
    return this->orientation;
}

/**
 * @brief Set the position of the module on the x-axis.
 *
 * This refers to the position in the discretized grid of the stations table.
 * 
 * @param pos_x 
 */
void Module::setPosX(float pos_x){
    this->pos_x = pos_x;
}

/**
 * @brief Set the position of the module on the y-axis.
 * 
 * This refers to the position in the discretized grid of the stations table.
 * 
 * @param pos_y 
 */
void Module::setPosY(float pos_y){
    this->pos_y = pos_y;
}

/**
 * @brief Set the orientation of the module.
 *
 * Orientation is discretized in steps of 90° around the z axis, described by number 0, 1, 2, 3.
 * 
 * @param orientation 
 */
void Module::setOrientation(float orientation){
    this->orientation = orientation;
}

/**
 * @brief Return whether or not the base plate is included in the modules 3D mesh.
 *
 * Some 3D meshes of a module may already include the base plate. Those meshes have to be positioned lower on
 * the z-axis than meshes that don't include the base plate.
 * 
 * @return true 
 * @return false 
 */
bool Module::hasPlate(){
    return this->has_plate;
}

/**
 * @brief Return the 3D mesh of the module as a shape message object.
 * 
 * This method creates a shape message from the resource found through the mesh URL.
 * The sacling is always 1:1 so the mesh found at the url must already be properly scaled.
 * 
 * @return shape_msgs::Mesh 
 */
shape_msgs::Mesh Module::getMeshShapeMsg(){
    shape_msgs::Mesh module_shape_msg;
    if(this->mesh_url != ""){
        Eigen::Vector3d vectorScale(1, 1, 1);
        shapes::Mesh* module_mesh = shapes::createMeshFromResource(this->mesh_url, vectorScale);
        shapes::ShapeMsg module_mesh_msg;
        shapes::constructMsgFromShape(module_mesh, module_mesh_msg);
        module_shape_msg = boost::get<shape_msgs::Mesh>(module_mesh_msg);
    }
    return module_shape_msg;
}


/** Add a Collision Protection Volume ( CPV ) to the module.
 * 
 * @param id The id of the new CPV. This is the same id as the corresponding collision object.
 * @param shape_type The shape of the CPV (BOX, SPHERE, CYLINDER, CONE)
 * @param dim1 Different shapes have different dimensions. This is the firest dimension which corresponds to x for boxes, radius for spheres and height for cylinders or cones.
 * @param dim2 Second dimension which corresponds to y for boxes and radius for cylinders and cones.
 * @param dim3 Third dimension which corresponds to z for boxes.
 * @param pose The pose of the CPV in the module's coordinate frame.
*/
void Module::addCPV(std::string id, CPVShapes shape_type, geometry_msgs::Pose pose, double dim1, double dim2, double dim3){
    CPV cpv;
    cpv.id = id;
    cpv.shape = shape_type;
    cpv.pose = pose;
    if (cpv.shape == CPVShapes::BOX){
        cpv.dimensions.resize(3);
        cpv.dimensions[CPVBox::X] = dim1;
        cpv.dimensions[CPVBox::Y] = dim2;
        cpv.dimensions[CPVBox::Z] = dim3;
    } else if (cpv.shape == CPVShapes::SPHERE){
        cpv.dimensions.resize(1);
        cpv.dimensions[CPVSphere::SPHERE_RADIUS] = dim1;
    } else if (cpv.shape == CPVShapes::CYLINDER){
        cpv.dimensions.resize(2);
        cpv.dimensions[CPVCylinder::CYLINDER_HEIGHT] = dim1;
        cpv.dimensions[CPVCylinder::CYLINDER_RADIUS] = dim2;
    } else if (cpv.shape == CPVShapes::CONE){
        cpv.dimensions.resize(2);
        cpv.dimensions[CPVCone::CONE_HEIGHT] = dim1;
        cpv.dimensions[CPVCone::CONE_RADIUS] = dim2;
    }
    this->cpvs.push_back(cpv);
}

/**
 * @brief Return a vector containing all Collision Protection Volumina objects of this module.
 * 
 * @return std::vector<CPV> 
 */
std::vector<CPV> Module::getCPVs(){
    return this->cpvs;
}

/**
 * @brief Return a specific key point of this module.
 * 
 * @param key_point_id The numerical id of the key point.
 * @return geometry_msgs::Pose The key point as a pose.
 */
geometry_msgs::Pose Module::getKeyPoint(std::size_t key_point_id){
    return this->key_points[key_point_id];
}