#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <path_planning_ros_interface/Assemble_configurationAction.h>
#include <path_planning_ros_interface/PlanAction.h>
#include <nlohmann/json.hpp>


int main (int argc, char **argv)
{
  ros::init(argc, argv, "dummy_action_client");

  // create the action client
  // true causes the client to spin its own thread
  actionlib::SimpleActionClient<path_planning_ros_interface::Assemble_configurationAction> assemble_configuration_ac("assemble_configuration_action_server", true);
  actionlib::SimpleActionClient<path_planning_ros_interface::PlanAction> plan_ac("plan_action_server", true);

  ROS_INFO("Waiting for action server to start.");
  // wait for the action server to start
  assemble_configuration_ac.waitForServer(); //will wait for infinite time
  plan_ac.waitForServer();

  ROS_INFO("Action server started, sending goal.");
  // send a goal to the action
  // /ros_ws/src/...


    /*
    , 
          {
            "module_id": "magnet_extraction_unit",
            "3d_mesh": "/home/loris/catkin_ws/src/descriptions/units/magneteinheit.dae",
            "operation": "ADD",
            "pose": {
              "plate": {
                "x": 1,
                "y": 0
                },
                "orientation": 3
                }
              }



              {
          "module_id": "pressing_unit",
          "3d_mesh": "/home/loris/path_planning/src/descriptions/units/auspresseinheit.dae",
          "operation": "ADD", 
          "pose": {
            "plate": {
              "x": 1,
              "y": 1
              },
            "orientation": 3
            }
          }, 
          {
            "module_id": "magnet_extraction_unit",
            "3d_mesh": "/home/loris/path_planning/src/descriptions/units/magneteinheit.dae",
            "operation": "ADD",
            "pose": {
              "plate": {
                "x": 1,
                "y": 0
                },
                "orientation": 3
                }
              }
    */
  //http://172.22.192.101:8049/stator_tube_cad/auspresseinheit.dae
  //http://172.22.192.101:8049/stator_tube_cad/magneteinheit.dae
  nlohmann::json assemble_configuration_goal_json = nlohmann::json::parse(R"({
    "table_config": [
      {
        "id": 1,
        "operation": "ADD",
        "position": {
          "x": 0,
          "y": 1
          }
        }
      ],
      "modules": [
        {
          "module_id": "pressing_unit",
          "3d_mesh": "http://172.22.192.101:8049/stator_tube_cad/auspresseinheit.dae",
          "operation": "ADD", 
          "pose": {
            "plate": {
              "x": 1,
              "y": 1
              },
            "orientation": 3
            }
          }, 
          {
            "module_id": "magnet_extraction_unit",
            "3d_mesh": "http://172.22.192.101:8049/stator_tube_cad/magneteinheit.dae",
            "operation": "ADD",
            "pose": {
              "plate": {
                "x": 1,
                "y": 0
                },
                "orientation": 3
                }
              }
          ],
          "robot": {
            "position": {
              "x": -0.113,
              "y": -0.887
            }
          }
        })");

  path_planning_ros_interface::Assemble_configurationGoal assemble_configuration_goal;
  assemble_configuration_goal.configuration = assemble_configuration_goal_json.dump();
  auto start = std::chrono::high_resolution_clock::now();
  assemble_configuration_ac.sendGoal(assemble_configuration_goal);

  //wait for the action to return
  bool finished_before_timeout = assemble_configuration_ac.waitForResult(ros::Duration(30.0));

  if (finished_before_timeout)
  {
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    actionlib::SimpleClientGoalState state = assemble_configuration_ac.getState();
    ROS_INFO("Action finished: %s in %li milliseconds",state.toString().c_str(), duration.count());
    path_planning_ros_interface::Assemble_configurationResult result = *(assemble_configuration_ac.getResult());
    std::string result_string = result.result;
    ROS_INFO("Action result: %s", result_string.c_str());
  }
  else
    ROS_INFO("Action did not finish before the time out.");

  ros::Duration(4).sleep();

  /*
  "x": -0.005,
                "y": -0.1,*/

/*
CALIBRATION
=======================================
{
            "gripper_state": 0,
            "linear_path": true,
            "object_gripped": false,
            "type": "GLOBAL_POSE",
            "orientation": {
                "w": 0.0,
                "x": 0.7,
                "y": 0.7,
                "z": 0.0
            },
            "position": {
                "x": -0.25,
                "y": -0.25,
                "z": 0.1
            }
        },
        {
            "gripper_state": 0,
            "linear_path": true,
            "object_gripped": false,
            "type": "GLOBAL_POSE",
            "orientation": {
                "w": 0.0,
                "x": 0.7,
                "y": 0.7,
                "z": 0.0
            },
            "position": {
                "x": -0.25,
                "y": 0.25,
                "z": 0.1
            }
        },
        {
            "gripper_state": 0,
            "linear_path": true,
            "object_gripped": false,
            "type": "GLOBAL_POSE",
            "orientation": {
                "w": 0.0,
                "x": 0.7,
                "y": 0.7,
                "z": 0.0
            },
            "position": {
                "x": 0.0,
                "y": 0.0,
                "z": 0.1
            }
        }
*/

/*
{
            "type": "JOINT_STATE",
            "gripper_state": 0.0,
            "object_gripped": true,
            "values": {
                "shoulder_pan_joint": 2.721830129623413,
                "shoulder_lift_joint": -1.3516109746745606,
                "elbow_joint": -2.57492733001709,
                "wrist_1_joint": -2.29378666500234,
                "wrist_2_joint": -0.4721172491656702,
                "wrist_3_joint": -3.190343443547384
            }
        },
        {
            "type": "JOINT_STATE",
            "gripper_state": 0.0,
            "object_gripped": true,
            "values": {
                "shoulder_pan_joint": 2.721830129623413,
                "shoulder_lift_joint": -1.3516109746745606,
                "elbow_joint": -2.57492733001709,
                "wrist_1_joint": -2.29378666500234,
                "wrist_2_joint": -0.4721172491656702,
                "wrist_3_joint": -3.190343443547384
            }
        },
        {
            "type": "JOINT_STATE",
            "gripper_state": 0.0,
            "object_gripped": true,
            "values": {
                "shoulder_pan_joint": 1.9496474266052246,
                "shoulder_lift_joint": -1.656938215295309,
                "elbow_joint": -2.4786062240600586,
                "wrist_1_joint": -2.142125745812887,
                "wrist_2_joint": -1.184112850819723,
                "wrist_3_joint": -3.138798777257101
            }
        },
        {
            "type": "JOINT_STATE",
            "gripper_state": 0.0,
            "object_gripped": true,
            "values": {
                "shoulder_pan_joint": 1.9496474266052246,
                "shoulder_lift_joint": -1.656938215295309,
                "elbow_joint": -2.4786062240600586,
                "wrist_1_joint": -2.142125745812887,
                "wrist_2_joint": -1.184112850819723,
                "wrist_3_joint": -3.138798777257101
            }
        },
        {
            "type": "JOINT_STATE",
            "gripper_state": 0.0,
            "object_gripped": true,
            "values": {
                "shoulder_pan_joint": 2.721830129623413,
                "shoulder_lift_joint": -1.3516109746745606,
                "elbow_joint": -2.57492733001709,
                "wrist_1_joint": -2.29378666500234,
                "wrist_2_joint": -0.4721172491656702,
                "wrist_3_joint": -3.190343443547384
            }
        },
        {
            "type": "JOINT_STATE",
            "gripper_state": 0.0,
            "object_gripped": true,
            "values": {
                "shoulder_pan_joint": 2.721830129623413,
                "shoulder_lift_joint": -1.3516109746745606,
                "elbow_joint": -2.57492733001709,
                "wrist_1_joint": -2.29378666500234,
                "wrist_2_joint": -0.4721172491656702,
                "wrist_3_joint": -3.190343443547384
            }
        },
        {
          "gripper_state": 0,
          "linear_path": false,
          "module_id": "magnet_extraction_unit",
          "object_gripped": true,
          "position": {
              "x": 0.2,
              "y": 0.020,
              "z": 0.25
          },
          "orientation": {
              "x": 0.7,
              "y": 0.0,
              "z": -0.7,
              "w": 0.0
          },
          "type": "MODULE_POSE"
        },
        {
          "gripper_state": 0,
          "linear_path": true,
          "module_id": "magnet_extraction_unit",
          "object_gripped": true,
          "position": {
              "x": 0.064,
              "y": 0.016,
              "z": 0.25
          },
          "orientation": {
              "x": 0.7,
              "y": 0.0,
              "z": -0.7,
              "w": 0.0
          },
          "type": "MODULE_POSE"
        },
        {
          "gripper_state": 0,
          "linear_path": true,
          "module_id": "magnet_extraction_unit",
          "object_gripped": false,
          "position": {
              "x": 0.064,
              "y": 0.016,
              "z": 0.197
          },
          "orientation": {
              "x": 0.7,
              "y": 0.0,
              "z": -0.7,
              "w": 0.0
          },
          "type": "MODULE_POSE"
        },
        {
          "gripper_state": 0,
          "linear_path": true,
          "module_id": "magnet_extraction_unit",
          "object_gripped": false,
          "position": {
              "x": 0.064,
              "y": 0.016,
              "z": 0.25
          },
          "orientation": {
              "x": 0.7,
              "y": 0.0,
              "z": -0.7,
              "w": 0.0
          },
          "type": "MODULE_POSE"
        },
        {
          "gripper_state": 0,
          "linear_path": true,
          "module_id": "magnet_extraction_unit",
          "object_gripped": true,
          "position": {
              "x": 0.2,
              "y": 0.020,
              "z": 0.25
          },
          "orientation": {
              "x": 0.7,
              "y": 0.0,
              "z": -0.7,
              "w": 0.0
          },
          "type": "MODULE_POSE"
        },
    ===============================================

    ,
        {
            "gripper_state": 0,
            "linear_path": true,
            "module_id": "pressing_unit",
            "object_gripped": false,
            "orientation": {
                "w": -0.7,
                "x": 0,
                "y": 0.7,
                "z": 0.0
            },
            "position": {
                "x": -0.006,
                "y": 0.021,
                "z": 0.41
            },
            "type": "MODULE_POSE"
        },
        {
            "gripper_state": 0,
            "linear_path": true,
            "module_id": "pressing_unit",
            "object_gripped": false,
            "orientation": {
                "w": -0.7,
                "x": 0,
                "y": 0.7,
                "z": 0.0
            },
            "position": {
                "x": -0.006,
                "y": 0.021,
                "z": 0.33
            },
            "type": "MODULE_POSE"
        },
        {
            "gripper_state": 0,
            "linear_path": true,
            "module_id": "pressing_unit",
            "object_gripped": false,
            "orientation": {
                "w": -0.7,
                "x": 0,
                "y": 0.7,
                "z": 0.0
            },
            "position": {
                "x": -0.006,
                "y": 0.021,
                "z": 0.33
            },
            "type": "MODULE_POSE"
        },
        {
            "gripper_state": 0,
            "linear_path": true,
            "module_id": "pressing_unit",
            "object_gripped": false,
            "orientation": {
                "w": -0.7,
                "x": 0,
                "y": 0.7,
                "z": 0.0
            },
            "position": {
                "x": -0.006,
                "y": 0.021,
                "z": 0.41
            },
            "type": "MODULE_POSE"
        },
        {
            "gripper_state": 0,
            "linear_path": true,
            "module_id": "pressing_unit",
            "object_gripped": true,
            "orientation": {
                "w": -0.7,
                "x": 0,
                "y": 0.7,
                "z": 0.0
            },
            "position": {
                "x": 0.1,
                "y": 0.0,
                "z": 0.41
            },
            "type": "MODULE_POSE"
        },
        {
            "type": "JOINT_STATE",
            "gripper_state": 0.0,
            "object_gripped": true,
            "values": {
                "shoulder_pan_joint": 2.721830129623413,
                "shoulder_lift_joint": -1.3516109746745606,
                "elbow_joint": -2.57492733001709,
                "wrist_1_joint": -2.29378666500234,
                "wrist_2_joint": -0.4721172491656702,
                "wrist_3_joint": -3.190343443547384
            }
        }

*/

  nlohmann::json planning_goal_json = nlohmann::json::parse(R"(
    {
    "start_state": {
        "shoulder_pan_joint": 2.4899845123291016,
        "shoulder_lift_joint": -1.6620899639525355,
        "elbow_joint": -2.60892391204834,
        "wrist_1_joint": 1.003727837199829,
        "wrist_2_joint": 1.055844783782959,
        "wrist_3_joint": 3.2385025024414062
    },
    "targets": [
        {
            "gripper_state": 0,
            "linear_path": false,
            "object_gripped": false,
            "type": "MODULE_POSE",
            "module_id": "pressing_unit",
            "orientation": {
                "w": 0.0,
                "x": 0.7,
                "y": 0.7,
                "z": 0.0
            },
            "position": {
                "x": 0.5,
                "y": 0.5,
                "z": 0.15
            }
        }
    ]
}
  )");
  
  path_planning_ros_interface::PlanGoal planning_goal;
  planning_goal.planning_request = planning_goal_json.dump();
  ROS_INFO("Send Goal");
  start = std::chrono::high_resolution_clock::now();
  plan_ac.sendGoal(planning_goal);

  finished_before_timeout = plan_ac.waitForResult(ros::Duration(300.0));

  if (finished_before_timeout)
  {
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    actionlib::SimpleClientGoalState state = plan_ac.getState();
    ROS_INFO("Action finished: %s in %li milliseconds",state.toString().c_str(), duration.count());
    path_planning_ros_interface::PlanResult result = *(plan_ac.getResult());
    nlohmann::json result_json = nlohmann::json::parse(result.result);
    std::string result_string;
    if(result_json["success"]){
      //result_string = result_json["id"].dump();
      result_string = result_json.dump(4);
    } else{
      result_string = result_json.dump();
    }
    ROS_INFO("Action result: %s", result_string.c_str());
  }
  else
    ROS_INFO("Action did not finish before the time out.");

  ros::Duration(4).sleep();

  //http://172.22.192.101:8049/stator_tube_cad/auspresseinheit.dae

  assemble_configuration_goal_json = nlohmann::json::parse(R"({
      "modules": [
        {
          "module_id": "pressing_unit",
          "3d_mesh": "file:///home/loris/catkin_ws/src/descriptions/units/auspresseinheit.dae",
          "operation": "UPDATE", 
          "pose": {
            "plate": {
              "x": 1,
              "y": 1
              },
            "orientation": 2
            }
          }
          ]
        })");

  assemble_configuration_goal;
  assemble_configuration_goal.configuration = assemble_configuration_goal_json.dump();
  start = std::chrono::high_resolution_clock::now();
  assemble_configuration_ac.sendGoal(assemble_configuration_goal);

  //wait for the action to return
  finished_before_timeout = assemble_configuration_ac.waitForResult(ros::Duration(30.0));

  if (finished_before_timeout)
  {
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    actionlib::SimpleClientGoalState state = assemble_configuration_ac.getState();
    ROS_INFO("Action finished: %s in %li milliseconds",state.toString().c_str(), duration.count());
    path_planning_ros_interface::Assemble_configurationResult result = *(assemble_configuration_ac.getResult());
    std::string result_string = result.result;
    ROS_INFO("Action result: %s", result_string.c_str());
  }
  else
    ROS_INFO("Action did not finish before the time out.");

  ros::Duration(4).sleep();

  planning_goal_json = nlohmann::json::parse(R"(
    {
    "start_state": {
        "shoulder_pan_joint": 2.4899845123291016,
        "shoulder_lift_joint": -1.6620899639525355,
        "elbow_joint": -2.60892391204834,
        "wrist_1_joint": 1.003727837199829,
        "wrist_2_joint": 1.055844783782959,
        "wrist_3_joint": 3.2385025024414062
    },
    "targets": [
        {
            "gripper_state": 0,
            "linear_path": false,
            "object_gripped": false,
            "type": "MODULE_POSE",
            "module_id": "pressing_unit",
            "orientation": {
                "w": 0.0,
                "x": 0.7,
                "y": 0.7,
                "z": 0.0
            },
            "position": {
                "x": 0.5,
                "y": 0.5,
                "z": 0.15
            }
        }
    ]
}
  )");
  
  planning_goal;
  planning_goal.planning_request = planning_goal_json.dump();
  ROS_INFO("Send Goal");
  start = std::chrono::high_resolution_clock::now();
  plan_ac.sendGoal(planning_goal);

  finished_before_timeout = plan_ac.waitForResult(ros::Duration(300.0));

  if (finished_before_timeout)
  {
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    actionlib::SimpleClientGoalState state = plan_ac.getState();
    ROS_INFO("Action finished: %s in %li milliseconds",state.toString().c_str(), duration.count());
    path_planning_ros_interface::PlanResult result = *(plan_ac.getResult());
    nlohmann::json result_json = nlohmann::json::parse(result.result);
    std::string result_string;
    if(result_json["success"]){
      //result_string = result_json["id"].dump();
      result_string = result_json.dump(4);
    } else{
      result_string = result_json.dump();
    }
    ROS_INFO("Action result: %s", result_string.c_str());
  }
  else
    ROS_INFO("Action did not finish before the time out.");

  ros::Duration(4).sleep();

  return 0;
}