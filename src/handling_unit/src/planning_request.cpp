#include "handling_unit/planning_request.hpp"

/**
 * @brief Construct a new Planning Request:: Planning Request object
 * 
 * @param env Pointer to the robots Environment object.
 */
PlanningRequest::PlanningRequest(std::shared_ptr<Environment> env){
    this->env = env;
}

/**
 * @brief Destroy the Planning Request:: Planning Request object
 * 
 */
PlanningRequest::~PlanningRequest(){
}

/**
 * @brief Initialize a PlanningRequest object from a received planning request in json format.
 * 
 * This method takes a json object and buildsa a PlanningRequest object from it.
 * Please see the schema defined in planning_request_schema.json to see how a planning request in json format should look like.
 * 
 * @param goal_json Incoming json.
 */
void PlanningRequest::initPlanningRequest(nlohmann::json goal_json){
    // working directory is /home/loris/catkin_ws/devel/lib/handling_unit
    std::ifstream file("../../../src/handling_unit/json_interface/planning_request_schema.json");
    std::stringstream string_buffer;
    string_buffer << file.rdbuf();
    file.close();
    nlohmann::json json_schema = nlohmann::json::parse(string_buffer.str());
    valijson::Schema schema;
    valijson::SchemaParser parser;
    valijson::adapters::NlohmannJsonAdapter schema_document_adapter(json_schema);
    try{
        parser.populateSchema(schema_document_adapter, schema);
    } catch(std::exception &e) {
        throw std::runtime_error("Could not parse the validation schema. " + (std::string)e.what());
    }
    valijson::Validator validator;
    valijson::adapters::NlohmannJsonAdapter target_document_adapter(goal_json);
    valijson::ValidationResults results;
    if(!validator.validate(schema, target_document_adapter, &results)){
        valijson::ValidationResults::Error error;
        std::string error_string = "Validation of schema failed for planning request, here is why: \n";
        while(results.popError(error)){
            std::string context;
            std::vector<std::string>::iterator itr = error.context.begin();
            for (; itr != error.context.end(); itr++) {
                context += *itr;
            }
            error_string += ("context: " + context + "\ndescription: " + error.description + "\n");
        }
        throw std::runtime_error(error_string);
    }
    if (goal_json.contains("start_state") && goal_json["start_state"].dump() != "{}") {
        this->setStartJointValues(goal_json);
    }
    for(auto& target : goal_json["targets"].items()){
        this->addTarget(target.value());
    }
}

/**
 * @brief Return a vector with pointers to all the Target objects in this PlanningRequest
 * 
 * @return std::vector<std::shared_ptr<Target>> 
 */
std::vector<std::shared_ptr<Target>> PlanningRequest::getTargets(){
    return this->targets;
}

/**
 * @brief Set the joint anlge values that make up the robots start state.
 * 
 * @param goal_json Incoming json.
 */
void PlanningRequest::setStartJointValues(nlohmann::json goal_json){
    std::vector<std::string> joint_names; 
    joint_names.push_back("shoulder_pan_joint");
    joint_names.push_back("shoulder_lift_joint");
    joint_names.push_back("elbow_joint");
    joint_names.push_back("wrist_1_joint");
    joint_names.push_back("wrist_2_joint");
    joint_names.push_back("wrist_3_joint");
    std::vector<double> joint_value_buffer = this->getJointValues(goal_json["start_state"]);
    this->start_joint_values.clear();
    for(double value : joint_value_buffer){
        this->start_joint_values.push_back(value);
    }
    for (std::size_t i = 0; i < joint_names.size(); ++i)
    {
        ROS_INFO("Joint %s: %lf", joint_names[i].c_str(), (this->start_joint_values)[i]);
    }
}

/**
 * @brief Return the joint angle values of the robots start state.
 * 
 * @return std::vector<double> 
 */
std::vector<double> PlanningRequest::getStartJointValues(){
    return this->start_joint_values;
}

/**
 * @brief Read the joint angle values for the robots start state from the json object.
 * 
 * @param json Incoming json.
 * @return std::vector<double> 
 */
std::vector<double> PlanningRequest::getJointValues(nlohmann::json json){
    std::vector<std::string> joint_names; 
    joint_names.push_back("shoulder_pan_joint");
    joint_names.push_back("shoulder_lift_joint");
    joint_names.push_back("elbow_joint");
    joint_names.push_back("wrist_1_joint");
    joint_names.push_back("wrist_2_joint");
    joint_names.push_back("wrist_3_joint");
    std::vector<double> joint_value_buffer;
    //std::string error_info;
    for(std::string joint_name : joint_names){
            try{
               joint_value_buffer.push_back((double)json[joint_name]);
            } catch(nlohmann::json::exception const& json_ex){
                throw std::runtime_error("Error in JSON while trying to get joint value for joint " + joint_name + ".\n" + (std::string)json_ex.what());
            } catch(std::invalid_argument const& inval_arg_ex){
                throw std::runtime_error("Invalid value in start_state for joint " + joint_name + ". Expected a numerical value.\n" + (std::string)inval_arg_ex.what());
            } catch(std::exception const& ex){
                throw std::runtime_error("Error while processing joint value for joint " + joint_name + ".\n" + (std::string)ex.what());
            }
    }
    return joint_value_buffer;
}

/**
 * @brief Read the target pose from the json object.
 * 
 * @param json Incoming json.
 * @return geometry_msgs::Pose 
 */
geometry_msgs::Pose PlanningRequest::getTargetPose(nlohmann::json json){
    geometry_msgs::Pose target_pose;
    try{
        target_pose.position.x = (double)json["position"]["x"];
        ROS_INFO_NAMED("planning_server", "Target position x: %f", target_pose.position.x);
        target_pose.position.y = (double)json["position"]["y"];
        ROS_INFO_NAMED("planning_server", "Target position y: %f", target_pose.position.y);
        target_pose.position.z = (double)json["position"]["z"];
        ROS_INFO_NAMED("planning_server", "Target position z: %f", target_pose.position.z);
        target_pose.orientation.x = (double)json["orientation"]["x"];
        ROS_INFO_NAMED("planning_server", "Target orientation x: %f", target_pose.orientation.x);
        target_pose.orientation.y = (double)json["orientation"]["y"];
        ROS_INFO_NAMED("planning_server", "Target orientation y: %f", target_pose.orientation.y);
        target_pose.orientation.z = (double)json["orientation"]["z"];
        ROS_INFO_NAMED("planning_server", "Target orientation z: %f", target_pose.orientation.z);
        target_pose.orientation.w = (double)json["orientation"]["w"];
        ROS_INFO_NAMED("planning_server", "Target orientation w: %f", target_pose.orientation.w);
    } catch(nlohmann::json::exception const& json_ex){
        throw std::runtime_error("Error in JSON while trying to get pose.\n" + (std::string)json_ex.what());
    } catch(std::invalid_argument const& inval_arg_ex){
        throw std::runtime_error("Invalid value in pose. Expected a numerical value.\n" + (std::string)inval_arg_ex.what());
    } catch(std::exception const& ex){
        throw std::runtime_error("Error while processing pose.\n" + (std::string)ex.what());
    }
    return target_pose;
}

/**
 * @brief Read the gripper joint angle from the json object.
 * 
 * @param target_json Json object of a single target.
 * @return double 
 */
double PlanningRequest::extractGripperState(nlohmann::json target_json){
    double gripper_state;
    try{
       gripper_state = (double)target_json["gripper_state"];
    } catch(nlohmann::json::exception const& json_ex){
        throw std::runtime_error("Error in JSON while trying to get gripper state.\n" + (std::string)json_ex.what());
    } catch(std::invalid_argument const& inval_arg_ex){
        throw std::runtime_error("Invalid value in gripper_state. Expected a numerical value.\n" + (std::string)inval_arg_ex.what());
    } catch(std::exception const& ex){
        throw std::runtime_error("Error while processing gripper_state.\n" + (std::string)ex.what());
    }
    return gripper_state;
}

/**
 * @brief Read whether or not the robots hold an object in its gripper when planning to the next target.
 * 
 * @param target_json Json object of a single target.
 * @return true 
 * @return false 
 */
bool PlanningRequest::extractObjectGripped(nlohmann::json target_json){
    bool object_gripped;
    try{
       object_gripped = (bool)target_json["object_gripped"];
    } catch(nlohmann::json::exception const& json_ex){
        throw std::runtime_error("Error in JSON while trying to get object_gripped.\n" + (std::string)json_ex.what());
    } catch(std::invalid_argument const& inval_arg_ex){
        throw std::runtime_error("Invalid value in object_gripped. Expected a boolean value.\n" + (std::string)inval_arg_ex.what());
    } catch(std::exception const& ex){
        throw std::runtime_error("Error while processing object_gripped\n" + (std::string)ex.what());
    }
    return object_gripped;
}

/**
 * @brief Add a STATE_TARGET to the PlanningRequest.
 * 
 * @param target_json Json object of a single target.
 */
void PlanningRequest::addStateTarget(nlohmann::json target_json){
    std::shared_ptr<Target> target = std::make_shared<Target>(TargetTypes::STATE_TARGET);
    target->setJointValues(this->getJointValues(target_json["values"]));
    target->setGripperState(extractGripperState(target_json));
    target->setObjectGripped(extractObjectGripped(target_json));
    this->targets.push_back(target);
}

/**
 * @brief Add a GLOBAL_POSE_TARGET to the PlanningRequest.
 * 
 * This method builds a Target object from the json object and performs a transformation of the target pose from the global coordinate frame to the base_link coordinate frame.
 * 
 * @param target_json Json object of a single target.
 */
void PlanningRequest::addGlobalPoseTarget(nlohmann::json target_json){
    std::shared_ptr<Target> target = std::make_shared<Target>(TargetTypes::GLOBAL_POSE_TARGET);
    geometry_msgs::PoseStamped pose_buffer;
    pose_buffer.header.frame_id = "global";
    pose_buffer.pose = this->getTargetPose(target_json);
    try{
    this->env->getTransformBuffer()->transform(pose_buffer, pose_buffer, "base_link");
    } catch (tf2::TransformException &ex) {
                throw std::runtime_error("Could not perform transform from global --> base_link.\n" + (std::string)ex.what());
    }
    target->setPose(pose_buffer);
    target->setGripperState(extractGripperState(target_json));
    target->setObjectGripped(extractObjectGripped(target_json));
    this->targets.push_back(target);
}

/**
 * @brief Add a MODULE_POSE_TARGET to the PlanningRequest.
 * 
 * This method builds a Target object from the json object and performs a transformation of the target pose from the modules coordinate frame to the base_link coordinate frame.
 * 
 * @param target_json Json object of a single target.
 */
void PlanningRequest::addModulePoseTarget(nlohmann::json target_json){
    std::shared_ptr<Target> target = std::make_shared<Target>(TargetTypes::MODULE_POSE_TARGET);
    std::string module_id = target_json["module_id"].dump();
    module_id.erase(remove(module_id.begin(), module_id.end(), '\"'), module_id.end());
    geometry_msgs::PoseStamped pose_buffer;
    if (target_json.contains("point_id")){
        std::size_t key_point_id = (std::size_t)target_json["point_id"];
        pose_buffer = this->env->getKeyPoint(module_id, key_point_id);
    } else {
        pose_buffer.header.frame_id = module_id;
        pose_buffer.pose = this->getTargetPose(target_json);
    }
    try{
        this->env->getTransformBuffer()->transform(pose_buffer, pose_buffer, "base_link");
        } catch (tf2::TransformException &ex) {
                throw std::runtime_error("Could not perform transform from " + module_id + " --> base_link.\n" + (std::string)ex.what());
    }
    ROS_INFO_NAMED("planning_server", "TARGET IN BASE LINK FRAME x: %f", pose_buffer.pose.position.x);
    ROS_INFO_NAMED("planning_server", "TARGET IN BASE LINK FRAME y: %f", pose_buffer.pose.position.y);
    ROS_INFO_NAMED("planning_server", "TARGET IN BASE LINK FRAME z: %f", pose_buffer.pose.position.z);
    ROS_INFO_NAMED("planning_server", "TARGET IN BASE LINK FRAME x: %f", pose_buffer.pose.orientation.x);
    ROS_INFO_NAMED("planning_server", "TARGET IN BASE LINK FRAME y: %f", pose_buffer.pose.orientation.y);
    ROS_INFO_NAMED("planning_server", "TARGET IN BASE LINK FRAME z: %f", pose_buffer.pose.orientation.z);
    ROS_INFO_NAMED("planning_server", "TARGET IN BASE LINK FRAME w: %f", pose_buffer.pose.orientation.w);
    target->setPose(pose_buffer);
    target->setLinearPath(((bool)target_json["linear_path"]));
    target->setGripperState(extractGripperState(target_json));
    target->setObjectGripped(extractObjectGripped(target_json));
    this->targets.push_back(target);
}

/**
 * @brief Add a Target to the PlanningRequest.
 * 
 * @param target_json Json object of a single target.
 */
void PlanningRequest::addTarget(nlohmann::json target_json){
    ROS_INFO("Process Target");
    std::string type = target_json["type"].dump();
    type.erase(remove(type.begin(), type.end(), '\"'), type.end());
    if(type == "JOINT_STATE"){
        addStateTarget(target_json);
    } else if (type == "GLOBAL_POSE"){
        addGlobalPoseTarget(target_json);
    } else if (type == "MODULE_POSE"){
        addModulePoseTarget(target_json);
    }  
}


