#include <ros/ros.h>
#include <string>
#include "geometric_shapes/shapes.h"
#include "geometric_shapes/mesh_operations.h"
#include "geometric_shapes/shape_operations.h"
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <geometric_shapes/shape_operations.h>
#include <moveit/moveit_cpp/moveit_cpp.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <eigen_conversions/eigen_msg.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <tf_conversions/tf_eigen.h>
#include <actionlib/server/simple_action_server.h>
#include <path_planning_ros_interface/PlanAction.h>
#include <handling_unit/assemble_configuration_action_server.hpp>
#include <handling_unit/plan_action_server.hpp>
#include <stdlib.h>
#include <moveit/moveit_cpp/moveit_cpp.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <handling_unit/environment.hpp>



int main(int argc, char** argv)
{
    ros::init(argc, argv, "path_planning");
    ros::NodeHandle node_handle("/path_planning");
    ros::AsyncSpinner spinner(2);
    spinner.start();
    ROS_WARN("PATH PLANNING");
    static const std::string PLANNING_GROUP = "arm";
    ros::Duration(1.0).sleep();
    auto moveit_cpp_ptr = std::make_shared<moveit_cpp::MoveItCpp>(node_handle);
    auto planning_component = std::make_shared<moveit_cpp::PlanningComponent>(PLANNING_GROUP, moveit_cpp_ptr);
    moveit::planning_interface::MoveGroupInterface move_group_interface(PLANNING_GROUP);
    planning_scene_monitor::PlanningSceneMonitorPtr psm = moveit_cpp_ptr->getPlanningSceneMonitorNonConst();
    psm->startSceneMonitor();
    psm->startWorldGeometryMonitor();
    psm->startStateMonitor();
    psm->startPublishingPlanningScene(planning_scene_monitor::PlanningSceneMonitor::SceneUpdateType::UPDATE_SCENE);
    psm->providePlanningSceneService();
    std::shared_ptr<moveit::planning_interface::PlanningSceneInterface> planning_scene_interface = std::make_shared<moveit::planning_interface::PlanningSceneInterface>();
    std::shared_ptr<Environment> env = std::make_shared<Environment>(move_group_interface.getPlanningFrame(), planning_scene_interface, psm);
    AssembleConfigurationActionServer assemble_configuration_as("assemble_configuration_action_server", env);
    PlanActionServer plan_as("plan_action_server", moveit_cpp_ptr, &node_handle, env, planning_component);
    ros::waitForShutdown();
    return 0;
}