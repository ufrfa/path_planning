#include "handling_unit/environment.hpp"

/**
 * @brief Construct a new Environment:: Environment object
 * 
 * @param frame_id The planning coordinate frame (base_link).
 * @param planning_scene_interface Pointer to the planning scene interface to interact with the robots planning scene.
 * @param psm Pointer to the planning scene monitor to keep track of the robots planning scene.
 */
Environment::Environment(std::string frame_id, std::shared_ptr<moveit::planning_interface::PlanningSceneInterface> planning_scene_interface, planning_scene_monitor::PlanningSceneMonitorPtr psm){
    this->frame_id = frame_id;
    this->planning_scene_interface = planning_scene_interface;
    this->tf_buffer_ptr = new tf2_ros::Buffer();
    this->tf_listener_ptr = new tf2_ros::TransformListener(*(this->tf_buffer_ptr));
    this->psm = psm;
}

/**
 * @brief Destroy the Environment:: Environment object
 * 
 */
Environment::~Environment(){}

/**
 * @brief Get a specific Table from the Environment
 * 
 * @param id The Table s identifier
 * @return Table 
 */
Table Environment::getTable(std::string id){
    return this->tables[id];
}

/**
 * @brief Get a specific Module from the Environment
 * 
 * @param id The Module s identifier
 * @return Module 
 */
Module Environment::getModule(std::string id){
    return this->modules[id];
}

/**
 * @brief Add a Table to the Environment.
 *
 * This will add the Table as a collision object to the robots planning scene.
 * 
 * @param table 
 */
void Environment::addTable(Table table){
    this->tables[table.id] = table;
    this->buildTable(table.id);
}

/**
 * @brief Add a Module to the Environment.
 * 
 * This will add the Module s 3D mesh as a collision object to the robots planning scene and publish its coordinate frame transformation.
 *
 * @param module 
 */
void Environment::addModule(Module module){
    ROS_WARN("ENV ADD MODULE");
    this->modules[module.id] = module;
    this->setModulePose(module.id, "global");
    this->setModuleFrameTransform(module.id);
    this->buildModule(module.id);
}

/**
 * @brief Set the pose of a Module in the planning coordinate frame from the modules position and orientation.
 * 
 * @param module_id The identifier of the Module.
 * @param pose_frame_id The coordinate frame of the Module.
 */
void Environment::setModulePose(std::string module_id, std::string pose_frame_id){
    Module *module = &(this->modules[module_id]);
    tf2::Quaternion orientation_quat;
    orientation_quat.setRPY(0, 0, (M_PI/2)*(module->getOrientation()+3));
    orientation_quat.normalize();
    module->module_pose.orientation.w = orientation_quat.getW();
    module->module_pose.orientation.x = orientation_quat.getX();
    module->module_pose.orientation.z = orientation_quat.getZ();
    module->module_pose.orientation.y = orientation_quat.getY();
    geometry_msgs::TransformStamped transform_base_link_global = this->getTransform(this->frame_id, pose_frame_id);
    module->module_pose.position.x = 0.5*module->pos_x + transform_base_link_global.transform.translation.x;
    module->module_pose.position.y = 0.5*module->pos_y + transform_base_link_global.transform.translation.y;
    if(module->has_plate) {
        module->module_pose.position.z = -0.0585;
        }
    else {
        module->module_pose.position.z = 0.0;
    }
}

/**
 * @brief Set up and publish the coordinate frame transformation between the planning coordinate frame and the modules coordinate frame.
 * 
 * @param module_id The identifier of the Module.
 */
void Environment::setModuleFrameTransform(std::string module_id){
    Module *module = &(this->modules[module_id]);
    static tf2_ros::StaticTransformBroadcaster static_broadcaster;
    module->module_frame_transform.header.stamp = ros::Time::now();
    module->module_frame_transform.header.frame_id = this->frame_id;
    module->module_frame_transform.child_frame_id = module->id;
    module->module_frame_transform.transform.translation.x = module->module_pose.position.x;
    module->module_frame_transform.transform.translation.y = module->module_pose.position.y;
    module->module_frame_transform.transform.translation.z = module->module_pose.position.z;
    module->module_frame_transform.transform.rotation.x = module->module_pose.orientation.x;
    module->module_frame_transform.transform.rotation.y = module->module_pose.orientation.y;
    module->module_frame_transform.transform.rotation.z = module->module_pose.orientation.z;
    module->module_frame_transform.transform.rotation.w = module->module_pose.orientation.w;
    ROS_WARN("SEND MODULE FRAME TRANSFORM");
    static_broadcaster.sendTransform(module->module_frame_transform);
}

/**
 * @brief Return the coordinate frame transformation buffer.
 * 
 * @return tf2_ros::Buffer* 
 */
tf2_ros::Buffer* Environment::getTransformBuffer(){
    return this->tf_buffer_ptr;
}

/**
 * @brief Get the transformation between two coordinate frames.
 * 
 * @param frame_id Parent coordinate frame (transformation from).
 * @param child_frame_id Child coordinate frame (transformation to).
 * @return geometry_msgs::TransformStamped 
 */
geometry_msgs::TransformStamped Environment::getTransform(std::string frame_id, std::string child_frame_id){
    ROS_INFO_NAMED("Module", "LOOK FOR TRANSFORM");
    geometry_msgs::TransformStamped transformStamped;
    try{
        transformStamped = this->tf_buffer_ptr->lookupTransform(frame_id, child_frame_id,
                                    ros::Time(0), ros::Duration(2.0));
    }
    catch (tf2::TransformException &ex) {
        ROS_INFO_NAMED("Module", "COULD NOT ACCESS TRANSFORMATION %s --> %s", frame_id.c_str(), child_frame_id.c_str());
        ROS_WARN("%s",ex.what());
        ros::Duration(1.0).sleep();
    }
    return transformStamped;
}

/**
 * @brief Build the robots planning scene with the elements in this Environment.
 * 
 */
void Environment::buildEnvironment(){
    for(const auto& i : this->tables){
        this->buildTable(i.first);
    }
    for(const auto& i : this->modules){
        this->buildModule(i.first);
    }
}

/**
 * @brief Add the Module s 3D mesh and its CPV s as collision objects to the robots planning scene.
 * 
 * @param id The Module s identifier.
 */
void Environment::buildModule(std::string id){
    Module* module = &(this->modules[id]);
    ROS_INFO_NAMED("ENVIRONMENT", "ADD MODULE");
    ROS_INFO_NAMED("ENVIRONMENT", "ID: %s", module->getId().c_str());
    moveit_msgs::CollisionObject module_collision_object;
    module_collision_object.header.frame_id = frame_id;
    module_collision_object.id = module->getId();
    shape_msgs::Mesh module_shape_msg = module->getMeshShapeMsg();
    std::vector<moveit_msgs::CollisionObject> collision_objects;
    module_collision_object.meshes.push_back(module_shape_msg);
    module_collision_object.mesh_poses.push_back(module->getModulePose());
    module_collision_object.operation = module_collision_object.ADD;
    collision_objects.push_back(module_collision_object);
    this->planning_scene_interface->addCollisionObjects(collision_objects);
    this->buildCollisionProtectionVolumina(*module);
    ROS_INFO_NAMED("ENVIRONMENT", "ADDED MODULE");
}

/**
 * @brief Remove all CPV s from the robots planning scene so that they are not considered for collision checking.
 * 
 */
void Environment::deactivateCPVs(){
    std::vector<std::string> cpv_ids;
    for(auto& module_itr : this->modules){
            for(auto& cpv : module_itr.second.getCPVs()){
                cpv_ids.push_back(cpv.id);
            }
        }
    this->planning_scene_interface->removeCollisionObjects(cpv_ids);
}

/**
 * @brief Add all the CPV s to the robots planning scene so that they are considered for collision checking.
 * 
 */
void Environment::activateCPVs(){
    for(auto& module_itr : this->modules){
        this->buildCollisionProtectionVolumina(module_itr.second);
    }
}

/**
 * @brief Add all the CPV s of a specific Module as collision objects to the robots planning scene.
 * 
 * @param module Reference to the Module
 */
void Environment::buildCollisionProtectionVolumina(Module& module){
    std::vector<moveit_msgs::CollisionObject> collision_objects;
    for(auto& cpv : module.getCPVs()){
        moveit_msgs::CollisionObject cpv_collision_object;
        cpv_collision_object.header.frame_id = module.id;
        cpv_collision_object.id = cpv.id;
        shape_msgs::SolidPrimitive cpv_primitive;
        switch (cpv.shape)
        {
        case CPVShapes::CYLINDER :
            cpv_primitive.type = cpv_primitive.CYLINDER;
            cpv_primitive.dimensions.resize(2);
            cpv_primitive.dimensions[cpv_primitive.CYLINDER_HEIGHT] = cpv.dimensions[CPVCylinder::CYLINDER_HEIGHT];
            cpv_primitive.dimensions[cpv_primitive.CYLINDER_RADIUS] = cpv.dimensions[CPVCylinder::CYLINDER_RADIUS];
            break;
        case CPVShapes::CONE :
            cpv_primitive.type = cpv_primitive.CONE;
            cpv_primitive.dimensions.resize(2);
            cpv_primitive.dimensions[cpv_primitive.CONE_HEIGHT] = cpv.dimensions[CPVCone::CONE_HEIGHT];
            cpv_primitive.dimensions[cpv_primitive.CONE_RADIUS] = cpv.dimensions[CPVCone::CONE_RADIUS];
            break;
        case CPVShapes::SPHERE :
            cpv_primitive.type = cpv_primitive.SPHERE;
            cpv_primitive.dimensions.resize(1);
            cpv_primitive.dimensions[cpv_primitive.SPHERE_RADIUS] = cpv.dimensions[CPVSphere::SPHERE_RADIUS];
            break;        
        default: //CPVShapes::BOX
            cpv_primitive.type = cpv_primitive.BOX;
            cpv_primitive.dimensions.resize(3);
            cpv_primitive.dimensions[cpv_primitive.BOX_X] = cpv.dimensions[CPVBox::X];
            cpv_primitive.dimensions[cpv_primitive.BOX_Y] = cpv.dimensions[CPVBox::Y];
            cpv_primitive.dimensions[cpv_primitive.BOX_Z] = cpv.dimensions[CPVBox::Z];
            break;
        }
        cpv_collision_object.primitives.push_back(cpv_primitive);
        cpv_collision_object.primitive_poses.push_back(cpv.pose);
        collision_objects.push_back(cpv_collision_object);
    }
    this->planning_scene_interface->addCollisionObjects(collision_objects);
}

/**
 * @brief Add a specific Table as a collision object to the robots planning scene.
 * 
 * @param id The Table s id
 */
void Environment::buildTable(std::string id){
    Table* table = &(this->tables[id]);
    float plate_pos_x = 0;
    float plate_pos_y = 0;
    if (table->pos_x != 0.0){
        plate_pos_x = 2*table->pos_x;
    }
    if (table->pos_y != 0.0){
        plate_pos_y = 2*this->tables[id].pos_y;
    }
    for(int i = 1; i <= 4; i++){
        BasePlate base_plate;
        base_plate.pos_x = plate_pos_x + (float)(i%2 == 0);
        base_plate.pos_y = plate_pos_y + (float)(i>=3);
        base_plate.id = id + "," + std::to_string(base_plate.pos_x) + "," + std::to_string(base_plate.pos_y);
        table->base_plates.push_back(base_plate);
        this->buildBasePlate(base_plate);
    }
}

/**
 * @brief Add a base plate as a collision object to the robots planning scene.
 * 
 * Base plates have measures 0.5m x 0.5m x 1.02m. This way, the space below a base plate is not reachable for the robot.
 * 
 * @param base_plate The base plate object that will be added
 */
void Environment::buildBasePlate(BasePlate& base_plate){
    ROS_INFO_NAMED("ENVIRONMENT", "ADD BASE PLATE");
    moveit_msgs::CollisionObject base_plate_collision_object;
    base_plate_collision_object.header.frame_id = this->frame_id;
    base_plate_collision_object.id = base_plate.id;
    ROS_INFO_NAMED("ENVIRONMENT", "BASE PLATE ID: %s", base_plate_collision_object.id.c_str());
    shape_msgs::SolidPrimitive base_plate_primitive;
    base_plate_primitive.type = base_plate_primitive.BOX;
    base_plate_primitive.dimensions.resize(3);
    base_plate_primitive.dimensions[base_plate_primitive.BOX_X] = 0.5;
    base_plate_primitive.dimensions[base_plate_primitive.BOX_Y] = 0.5;
    base_plate_primitive.dimensions[base_plate_primitive.BOX_Z] = 1.02;
    geometry_msgs::TransformStamped transform_base_link_global = this->getTransform("base_link", "global");
    geometry_msgs::Pose base_plate_pose;
    base_plate_pose.orientation.w = 1.0;
    base_plate_pose.position.x = base_plate.pos_x*0.5 + transform_base_link_global.transform.translation.x;
    base_plate_pose.position.y = base_plate.pos_y*0.5 + transform_base_link_global.transform.translation.y;
    base_plate_pose.position.z = -0.5101;
    ROS_INFO_NAMED("ENVIRONMENT", "BASE PLATE POSE X: %f", base_plate_pose.position.x);
    ROS_INFO_NAMED("ENVIRONMENT", "BASE PLATE POSE Y: %f", base_plate_pose.position.y);
    base_plate_collision_object.primitives.push_back(base_plate_primitive);
    base_plate_collision_object.primitive_poses.push_back(base_plate_pose);
    base_plate_collision_object.operation = base_plate_collision_object.ADD;
    std::vector<moveit_msgs::CollisionObject> base_plates;
    base_plates.push_back(base_plate_collision_object);
    this->planning_scene_interface->addCollisionObjects(base_plates);
    ROS_INFO_NAMED("ENVIRONMENT", "ADDED BASE PLATE");
}

/**
 * @brief Set the robots position in the global coordinate frame.
 * 
 * If there are no Table s present in the Environment, a Table is added at position (0,0).
 * 
 * @param pos_x 
 * @param pos_y 
 */
void Environment::addRobotPosition(float pos_x, float pos_y){
    static tf2_ros::StaticTransformBroadcaster static_broadcaster;
    geometry_msgs::TransformStamped transformStamped;
    transformStamped.header.stamp = ros::Time::now();
    transformStamped.header.frame_id = "base_link";
    transformStamped.child_frame_id = "global";
    tf2::Quaternion quat;
    quat.setRPY(0, 0, 0);
    transformStamped.transform.rotation.x = quat.x();
    transformStamped.transform.rotation.y = quat.y();
    transformStamped.transform.rotation.z = quat.z();
    transformStamped.transform.rotation.w = quat.w();
    transformStamped.transform.translation.x = pos_x + 0.25;
    transformStamped.transform.translation.y = pos_y + 0.25;
    static_broadcaster.sendTransform(transformStamped);
    if(this->tables.empty()){
        Table new_table;
        new_table.id = "0";
        new_table.pos_x = (float)0.0;
        new_table.pos_y = (float)0.0;
        this->addTable(new_table);
    }
}

/**
 * @brief Delete alle Table s and Module s from the environment and the robots planning scene.
 * 
 */
void Environment::reset(){
    ROS_INFO_NAMED("ENVIRONMENT", "RESET ENV");
    ROS_INFO_NAMED("ENVIRONMENT", "RESET ENV");
    ROS_INFO_NAMED("ENVIRONMENT", "RESET ENV");
    ROS_INFO_NAMED("ENVIRONMENT", "RESET ENV");
    std::vector<std::string> collision_object_ids;
    for(const auto& i : this->tables){
        for(const auto& base_plate : i.second.base_plates){
            collision_object_ids.push_back(base_plate.id);
        }
    }
    for(const auto& i : this->modules){
        collision_object_ids.push_back(i.first);
        for(CPV j : i.second.cpvs){
            collision_object_ids.push_back(j.id);
        }
    }
    this->planning_scene_interface->removeCollisionObjects(collision_object_ids);
    this->tables.clear();
    this->modules.clear();
    ROS_INFO_NAMED("ENVIRONMENT", "RESET ENV");
    ROS_INFO_NAMED("ENVIRONMENT", "RESET ENV");
    ROS_INFO_NAMED("ENVIRONMENT", "RESET ENV");
    ROS_INFO_NAMED("ENVIRONMENT", "RESET ENV");
}

/**
 * @brief Remove a specific Table from the Environment and the robots planning scene.
 * 
 * @param table_id The Table s id
 */
void Environment::removeTable(std::string table_id){
    std::vector<std::string> collision_object_ids;
    for(const auto& base_plate : this->tables[table_id].base_plates){
        collision_object_ids.push_back(base_plate.id);
    }
    this->planning_scene_interface->removeCollisionObjects(collision_object_ids);
    this->tables.erase(table_id);
}

/**
 * @brief Remova a specific Module from the Environment and the robots planning scene.
 * 
 * @param module_id The Module s id.
 */
void Environment::removeModule(std::string module_id){
    ROS_WARN("ENV REMOVE MODULE");
    std::vector<std::string> collision_object_ids;
    collision_object_ids.push_back(module_id);
    collision_object_ids.push_back(module_id + "_cpv_top");
    collision_object_ids.push_back(module_id + "_cpv_bottom");
    this->planning_scene_interface->removeCollisionObjects(collision_object_ids);
    this->modules.erase(module_id);
}

/**
 * @brief Check whether a specific RobotState is in collision.
 * 
 * For the optional parameters please see the documentation for <a href="http://docs.ros.org/en/indigo/api/moveit_core/html/structcollision__detection_1_1CollisionRequest.html">collision_detection::CollisionRequest</a>.
 * 
 * 
 * @param robot_state The RobotState that should be checked.
 * @param calculate_contacts Optional.
 * @param calculate_cost Optional.
 * @param calculate_distance Optional.
 * @param group_name Optional.
 * @return collision_detection::CollisionResult See <a href="http://docs.ros.org/en/indigo/api/moveit_core/html/structcollision__detection_1_1CollisionResult.html">collision_detection::CollisionResult</a>.
 */
collision_detection::CollisionResult Environment::checkCollision(robot_state::RobotState robot_state, 
        boost::optional<bool> calculate_contacts, 
        boost::optional<bool> calculate_cost,
        boost::optional<bool> calculate_distance, 
        boost::optional<std::string> group_name){
    planning_scene_monitor::LockedPlanningSceneRO locked_ps(this->psm);
    collision_detection::CollisionRequest collision_request;
    collision_request.contacts = calculate_contacts.value_or(false);
    collision_request.cost = calculate_cost.value_or(false);
    collision_request.distance = calculate_distance.value_or(false);
    collision_request.group_name = group_name.value_or("");
    collision_detection::CollisionResult collision_result;
    locked_ps->checkCollision(collision_request, collision_result, robot_state);
    return collision_result;
}

/**
 * @brief Get a specific Key Point from a specific Moudle in the Modules coordinate frame.
 * 
 * @param module_id The Module s id.
 * @param key_point_id The Key Points id.
 * @return geometry_msgs::PoseStamped The Key Point as Pose in the Module s coordinate frame.
 */
geometry_msgs::PoseStamped Environment::getKeyPoint(std::string module_id, std::size_t key_point_id){
    geometry_msgs::PoseStamped pose; 
    pose.pose = this->getModule(module_id).getKeyPoint(key_point_id);
    pose.header.frame_id = module_id;
    return pose;
}
