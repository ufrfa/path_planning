#include "handling_unit/assemble_configuration_action_server.hpp"

/**
 * @brief Construct a new Assemble Configuration Action Server:: Assemble Configuration Action Server object
 * 
 * @param name Name of the action. Should be "assemble_configuration_action_server".
 * @param env Pointer to the robots environment object.
 */
AssembleConfigurationActionServer::AssembleConfigurationActionServer(std::string name,std::shared_ptr<Environment> env) :
            as_(nh_, name, boost::bind(&AssembleConfigurationActionServer::executeCB, this, _1), false),
            action_name_(name){
                as_.start();
                this->env = env;
                ROS_INFO("Started Assemble configuration Action Server");
            }

/**
 * @brief Destroy the Assemble Configuration Action Server:: Assemble Configuration Action Server object
 * 
 */
AssembleConfigurationActionServer::~AssembleConfigurationActionServer(void){
}

/**
 * @brief Check the received goal json object acainst the schema defined in assembel_configuration_schema.json
 * 
 * @param goal_json The received goal as a json ojbect
 */
void AssembleConfigurationActionServer::validateJson(nlohmann::json goal_json){
    std::ifstream file("../../../src/handling_unit/json_interface/assemble_configuration_schema.json");
    std::stringstream string_buffer;
    string_buffer << file.rdbuf();
    file.close();
    nlohmann::json json_schema = nlohmann::json::parse(string_buffer.str());
    valijson::Schema schema;
    valijson::SchemaParser parser;
    valijson::adapters::NlohmannJsonAdapter schema_document_adapter(json_schema);
    try{
        parser.populateSchema(schema_document_adapter, schema);
    } catch(std::exception &e) {
        throw std::runtime_error("Could not parse the validation schema. " + (std::string)e.what());
    }
    valijson::Validator validator;
    valijson::adapters::NlohmannJsonAdapter target_document_adapter(goal_json);
    valijson::ValidationResults results;
    if(!validator.validate(schema, target_document_adapter, &results)){
        valijson::ValidationResults::Error error;
        std::string error_string = "Validation of schema failed for planning request, here is why: \n";
        while(results.popError(error)){
            std::string context;
            std::vector<std::string>::iterator itr = error.context.begin();
            for (; itr != error.context.end(); itr++) {
                context += *itr;
            }
            error_string += ("context: " + context + "\ndescription: " + error.description + "\n");
        }
        throw std::runtime_error(error_string);
    }
}

/**
 * @brief Set the action result to aborted.
 * 
 * @param error_info Error message why the action was aborted.
 */
void AssembleConfigurationActionServer::abortCB(std::string error_info){
    nlohmann::json result_json;
    result_json["success"] = false;
    result_json["error_info"] = error_info;
    result_.result = result_json.dump();
    as_.setAborted(result_);
}

/**
 * @brief ADD, REMOVE or UPDATE the Table s in the Environment according to the received goal json object.
 * 
 * @param goal_json 
 * @return true Build Table s successfully
 * @return false Could not build Table s successfully
 */
bool AssembleConfigurationActionServer::buildTables(nlohmann::json goal_json){
    std::unique_ptr<std::string> table_id = std::make_unique<std::string>("");
    std::unique_ptr<std::string> pos_x = std::make_unique<std::string>("");
    std::unique_ptr<std::string> pos_y = std::make_unique<std::string>("");
    std::unique_ptr<std::string> operation = std::make_unique<std::string>("");
    for(auto& table : goal_json["table_config"].items()){
        if(table.value().find("id") == table.value().end() 
            || table.value().find("operation") == table.value().end()){
            this->abortCB("Received invalid table configuration.");
            return false;
        }
        *table_id = table.value()["id"].dump();
        table_id->erase(remove(table_id->begin(), table_id->end(), '\"'), table_id->end());
        ROS_INFO("Table: %s", table_id->c_str());
        *operation = table.value()["operation"].dump();
        operation->erase(remove(operation->begin(), operation->end(), '\"'), operation->end());
        if (*table_id == "" || *operation == ""){
            this->abortCB("Received invalid table configuration.");
            return false;
        }
        if(*operation == "ADD"){
            if(table.value().find("position") == table.value().end() 
                || table.value()["position"].find("x") == table.value()["position"].end()
                || table.value()["position"].find("y") == table.value()["position"].end()){
            this->abortCB("Received invalid table configuration.");
            return false;
        }
            *pos_x = table.value()["position"]["x"].dump();
            pos_x->erase(remove(pos_x->begin(), pos_x->end(), '\"'), pos_x->end());
            ROS_INFO("Pos X: %s", pos_x->c_str());
            *pos_y = table.value()["position"]["y"].dump();
            pos_y->erase(remove(pos_y->begin(), pos_y->end(), '\"'), pos_y->end());
            ROS_INFO("Pos X: %s", pos_y->c_str());
            if (*pos_x == "" || *pos_y == ""){
                this->abortCB("Received invalid table configuration.");
                return false;
            }
            Table new_table;
            new_table.id = *table_id;
            new_table.pos_x = std::stof(*pos_x);
            new_table.pos_y = std::stof(*pos_y);
            this->env->addTable(new_table);
        } else if (*operation == "REMOVE"){
            this->env->removeTable(*table_id);
        } else if (*operation == "UPDATE"){
            if(table.value().find("position") == table.value().end()){
                this->abortCB("Received invalid table configuration.");
                return false;
            }
            Table current_table = this->env->getTable(*table_id);
            if(table.value()["positoin"].find("x") != table.value()["position"].end()){
                *pos_x = table.value()["position"]["x"].dump();
                pos_x->erase(remove(pos_x->begin(), pos_x->end(), '\"'), pos_x->end());
                current_table.pos_x = std::stof(*pos_x);
            }
            if(table.value()["positoin"].find("y") != table.value()["position"].end()){
                *pos_y = table.value()["position"]["y"].dump();
                pos_y->erase(remove(pos_y->begin(), pos_y->end(), '\"'), pos_y->end());
                current_table.pos_y = std::stof(*pos_y);
            }
            this->env->removeTable(*table_id);
            this->env->addTable(current_table);
        }
    }
    return true;
}

/**
 * @brief Add the CPV s to a specific Module according to the rosparams defined in cpv_params.yaml
 * 
 * @param module_id The id of the Module.
 * @param new_module The updated Module object with the new CPV s.
 */
void AssembleConfigurationActionServer::buildCPVs(std::string module_id, Module &new_module){
    XmlRpc::XmlRpcValue cpvs;
    if(!ros::param::get("path_planning/cpvs", cpvs)){
        ROS_WARN("COULD NOT READ ROS PARAMS FOR CPVS");
        return;
    }
    if(cpvs.getType() == XmlRpc::XmlRpcValue::Type::TypeStruct && cpvs.hasMember(module_id)){
        if(cpvs[module_id].getType() == XmlRpc::XmlRpcValue::Type::TypeArray && cpvs[module_id].size() > 0){
            for(int i = 0; i < cpvs[module_id].size(); i++){
                if(cpvs[module_id][i].getType() == XmlRpc::XmlRpcValue::Type::TypeStruct 
                    && cpvs[module_id][i].hasMember("position")
                    && cpvs[module_id][i].hasMember("dimensions")
                    && cpvs[module_id][i].hasMember("shape")
                    && cpvs[module_id][i].hasMember("id")){
                        geometry_msgs::Pose cpv_pose;
                        std::string cpv_id = module_id + "_" + (std::string)cpvs[module_id][i]["id"];
                        if(cpvs[module_id][i]["position"].getType() == XmlRpc::XmlRpcValue::Type::TypeStruct
                            && cpvs[module_id][i]["position"].hasMember("x")
                            && cpvs[module_id][i]["position"].hasMember("y")
                            && cpvs[module_id][i]["position"].hasMember("z")){
                                cpv_pose.position.x = cpvs[module_id][i]["position"]["x"];
                                cpv_pose.position.y = cpvs[module_id][i]["position"]["y"];
                                cpv_pose.position.z = cpvs[module_id][i]["position"]["z"];
                        }
                        if(cpvs[module_id][i]["dimensions"].getType() == XmlRpc::XmlRpcValue::Type::TypeArray && cpvs[module_id][i]["dimensions"].size() > 0){
                            if(cpvs[module_id][i]["shape"] == "CYLINDER")
                                new_module.addCPV(cpv_id, CPVShapes::CYLINDER, cpv_pose, double(cpvs[module_id][i]["dimensions"][0]), double(cpvs[module_id][i]["dimensions"][1]));
                            else if(cpvs[module_id][i]["shape"] == "BOX")
                                new_module.addCPV(cpv_id, CPVShapes::BOX, cpv_pose, double(cpvs[module_id][i]["dimensions"][0]), double(cpvs[module_id][i]["dimensions"][1]), double(cpvs[module_id][i]["dimensions"][2]));
                            else if(cpvs[module_id][i]["shape"] == "SPHERE")
                                new_module.addCPV(cpv_id, CPVShapes::SPHERE, cpv_pose, double(cpvs[module_id][i]["dimensions"][0]));
                            else if(cpvs[module_id][i]["shape"] == "CONE")
                                new_module.addCPV(cpv_id, CPVShapes::CONE, cpv_pose, double(cpvs[module_id][i]["dimensions"][0]), double(cpvs[module_id][i]["dimensions"][1]));
                        } 
                    }
                }
        }

    }
}

/**
 * @brief Add the key points defined a modules json description to the Module object.
 * 
 * @param new_module The updated Module with the new key points.
 * @param module_json The json description of the module.
 * @return true Could add key points successfully
 * @return false Could not add key points successfully
 */
bool AssembleConfigurationActionServer::addKeyPoints(Module &new_module, nlohmann::json module_json){
    for(auto& key_point : module_json["key_points"].items()){
                if (key_point.value().find("id") == key_point.value().end()
                    || key_point.value().find("position") == key_point.value().end()
                    || key_point.value().find("orientation") == key_point.value().end()
                    || key_point.value()["position"].find("x") == key_point.value()["position"].end()
                    || key_point.value()["position"].find("y") == key_point.value()["position"].end()
                    || key_point.value()["position"].find("z") == key_point.value()["position"].end()
                    || key_point.value()["orientation"].find("x") == key_point.value()["orientation"].end()
                    || key_point.value()["orientation"].find("y") == key_point.value()["orientation"].end()
                    || key_point.value()["orientation"].find("z") == key_point.value()["orientation"].end()
                    || key_point.value()["orientation"].find("w") == key_point.value()["orientation"].end()
                    ){
                this->abortCB("Received invalid key point configuration.");
                return false;
                }
                std::size_t key_point_id = (std::size_t)key_point.value()["id"];
                geometry_msgs::Pose key_point_pose;
                key_point_pose.position.x = (double)key_point.value()["position"]["x"];
                key_point_pose.position.y = (double)key_point.value()["position"]["y"];
                key_point_pose.position.z = (double)key_point.value()["position"]["z"];
                key_point_pose.orientation.x = (double)key_point.value()["orientation"]["x"];
                key_point_pose.orientation.y = (double)key_point.value()["orientation"]["y"];
                key_point_pose.orientation.z = (double)key_point.value()["orientation"]["z"];
                key_point_pose.orientation.w = (double)key_point.value()["orientation"]["w"];
                new_module.addKeyPoint(key_point_id, key_point_pose);
            }
    return true;
}

/**
 * @brief Build a Module object from a modules json description and add it to the Environment
 * 
 * @param module_json The modules json description
 * @param new_module_id The id of the new Module
 * @return true Could add Module successfully
 * @return false Could not add Module successfully
 */
bool AssembleConfigurationActionServer::addModule(nlohmann::json module_json, std::string new_module_id){
    if(module_json.find("pose") == module_json.end()
                || module_json["pose"].find("plate") == module_json["pose"].end()
                || module_json["pose"].find("orientation") == module_json["pose"].end()
                || module_json["pose"]["plate"].find("x") == module_json["pose"]["plate"].end()
                || module_json["pose"]["plate"].find("y") == module_json["pose"]["plate"].end()){
                    this->abortCB("Received invalid module configuration.");
                    return false;
            }
            std::unique_ptr<std::string> mesh = std::make_unique<std::string>("");
            std::unique_ptr<std::string> plate_pos_x = std::make_unique<std::string>("");
            std::unique_ptr<std::string> plate_pos_y = std::make_unique<std::string>("");
            std::unique_ptr<std::string> orientation_str = std::make_unique<std::string>("");
            std::unique_ptr<float> orientation = std::make_unique<float>();
            *mesh = module_json["3d_mesh"].dump();
            mesh->erase(remove(mesh->begin(), mesh->end(), '\"'), mesh->end());
            *plate_pos_x = module_json["pose"]["plate"]["x"].dump();
            plate_pos_x->erase(remove(plate_pos_x->begin(), plate_pos_x->end(), '\"'), plate_pos_x->end());
            *plate_pos_y = module_json["pose"]["plate"]["y"].dump();
            plate_pos_y->erase(remove(plate_pos_y->begin(), plate_pos_y->end(), '\"'), plate_pos_y->end());
            *orientation_str = module_json["pose"]["orientation"].dump();
            orientation_str->erase(remove(orientation_str->begin(), orientation_str->end(), '\"'), orientation_str->end());
            *orientation = std::stof(*orientation_str);
            Module new_module = Module(new_module_id, std::stof(*plate_pos_x), std::stof(*plate_pos_y), *orientation, true);
            new_module.setMeshURL(*mesh);
            this->buildCPVs(new_module_id, new_module);
            if(!this->addKeyPoints(new_module, module_json))
                return false;
            this->env->addModule(new_module);
            return true;
}

/**
 * @brief Update an existing Module object in the robots environment with the information in the modules json description.
 * 
 * @param module_json The modules json description.
 * @param current_module_id Id of the existing Module.
 * @return true Could update Module successfully
 * @return false Could not update Module successfully
 */
bool AssembleConfigurationActionServer::updateModule(nlohmann::json module_json, std::string current_module_id){
    ROS_WARN("UPDATE MODULE");
    Module current_module = this->env->getModule(current_module_id);
    if(module_json.find("pose") == module_json.end()){
            this->abortCB("Received invalid module configuration.");
            return false;
    }
    std::unique_ptr<std::string> plate_pos_x = std::make_unique<std::string>("");
    std::unique_ptr<std::string> plate_pos_y = std::make_unique<std::string>("");
    std::unique_ptr<std::string> orientation_str = std::make_unique<std::string>("");
    std::unique_ptr<float> orientation = std::make_unique<float>();
    if(module_json["pose"].find("orientation") != module_json["pose"].end()){
        *orientation_str = module_json["pose"]["orientation"].dump();
        orientation_str->erase(remove(orientation_str->begin(), orientation_str->end(), '\"'), orientation_str->end());
        *orientation = std::stof(*orientation_str);
        current_module.setOrientation(*orientation);
    }
    if(module_json["pose"].find("plate") != module_json["pose"].end()
        && module_json["pose"]["plate"].find("x") != module_json["pose"]["plate"].end()){
        *plate_pos_x = module_json["pose"]["plate"]["x"].dump();
        plate_pos_x->erase(remove(plate_pos_x->begin(), plate_pos_x->end(), '\"'), plate_pos_x->end());
        current_module.setPosX(std::stof(*plate_pos_x));
    }
    if(module_json["pose"].find("plate") != module_json["pose"].end()
        && module_json["pose"]["plate"].find("y") != module_json["pose"]["plate"].end()){
        *plate_pos_y = module_json["pose"]["plate"]["y"].dump();
        plate_pos_y->erase(remove(plate_pos_y->begin(), plate_pos_y->end(), '\"'), plate_pos_y->end());
        current_module.setPosY(std::stof(*plate_pos_y));
    }
    if(module_json.find("pose") == module_json.end()){
        current_module.clearKeyPoints();
        if(!this->addKeyPoints(current_module, module_json))
            return false;
    }
    this->env->removeModule(current_module_id);
    this->env->addModule(current_module);
    return true;
}

/**
 * @brief ADD, UPATE or REMOVE Modules according to the received goal json object.
 * 
 * @param goal_json The received goal json.
 * @return true Could perform operation successfully.
 * @return false Could not perform operation successfully.
 */
bool AssembleConfigurationActionServer::buildModules(nlohmann::json goal_json){
    std::unique_ptr<std::string> module_id = std::make_unique<std::string>("");
    std::unique_ptr<std::string> operation = std::make_unique<std::string>("");
    for(auto& module : goal_json["modules"].items()){
        if (module.value().find("module_id") == module.value().end()
                || module.value().find("operation") == module.value().end()){
            this->abortCB("Received invalid module configuration.");
            return false;
            }
        *module_id = module.value()["module_id"].dump();
        module_id->erase(remove(module_id->begin(), module_id->end(), '\"'), module_id->end());
        *operation = module.value()["operation"].dump();
        operation->erase(remove(operation->begin(), operation->end(), '\"'), operation->end());
        if (*module_id == "" || *operation == ""){ //|| *mesh == ""
            this->abortCB("Received invalid module configuration.");
            return false;
        }
        if (*operation == "ADD"){
            if(!this->addModule(module.value(), *module_id))
                return false;
        } else if (*operation == "REMOVE"){
            this->env->removeModule(*module_id);
        } else if (*operation == "UPDATE"){
            if(!this->updateModule(module.value(), *module_id))
                return false;
        }
    }
    return true;
}

/**
 * @brief The action callback function.
 *
 * This function reads the received action goal and manages the creation and adaption of the robots environment.
 * 
 * @param goal The received action goal.
 */
void AssembleConfigurationActionServer::executeCB(const path_planning_ros_interface::Assemble_configurationGoalConstPtr &goal){
    nlohmann::json result_json;
    if(as_.isPreemptRequested() || !ros::ok()){
        ROS_INFO("%s: Preempted", action_name_.c_str());
        result_json["success"] = false;
        result_json["error_info"] = "Action Preempted!";
        result_.result = result_json.dump();
        as_.setPreempted(result_);
        return;
    }
    feedback_.feedback = "Starting initialization.";
    as_.publishFeedback(feedback_);
    std::string goal_msg = goal->configuration;
    nlohmann::json goal_json;
    try{
        goal_json = nlohmann::json::parse(goal_msg);
    } catch (nlohmann::json::parse_error& ex) {
        this->abortCB("Received goal is not valid JSON and could not be parsed to string.\nError at position: " + std::to_string(ex.byte) + "\n" + ex.what());
        return;
    }
    try{
        this->validateJson(goal_json);
    } catch (std::exception& ex) {
        this->abortCB("Error while trying to validate request json. \n" + (std::string)ex.what());
        return;
    }
    if (goal_json.find("robot") != goal_json.end() 
            && goal_json["robot"].find("position") != goal_json["robot"].end() 
            && goal_json["robot"]["position"].find("x") != goal_json["robot"]["position"].end() 
            && goal_json["robot"]["position"].find("y") != goal_json["robot"]["position"].end()){
        ROS_WARN("Set Robot Position");
        this->env->addRobotPosition((float)goal_json["robot"]["position"]["x"], (float)goal_json["robot"]["position"]["y"]);
    }
    ROS_WARN("Build Tables");
    if(!this->buildTables(goal_json))
        return;
    ROS_WARN("Start Build Module");
    if(!this->buildModules(goal_json))
        return;
    result_json["success"] = true;
    result_json["error_info"] = "";
    result_.result = result_json.dump();
    as_.setSucceeded(result_);
}