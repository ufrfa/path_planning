<?xml version="1.0"?>
<robot xmlns:xacro="http://wiki.ros.org/xacro">
  <!--
    Convenience wrapper for the 'ur_robot' macro which provides default values
    for the various "parameters files" parameters for a UR10e.

    This file can be used when composing a more complex scene with one or more
    UR10e robots.

    While the generic 'ur_robot' macro could also be used, it would require
    the user to provide values for all parameters, as that macro does not set
    any model-specific defaults (not even for the generic parameters, such as
    the visual and physical parameters and joint limits).

    Refer to the main 'ur_macro.xacro' in this package for information about
    use, contributors and limitations.

    NOTE: users will most likely want to override *at least* the
          'kinematics_parameters_file' parameter. Otherwise, a default kinematic
          model will be used, which will almost certainly not correspond to any
          real robot.
  
  kinematics_parameters_file:='$(find ur_description)/config/ur10e/default_kinematics.yaml'
  
  -->
  <xacro:macro name="handling_unit" params="
   prefix
   joint_limits_parameters_file:='$(find ur_description)/config/ur10e/joint_limits.yaml'
   kinematics_parameters_file:='$(find handling_unit_description)/config/robot_calibration.yaml'
   physical_parameters_file:='$(find ur_description)/config/ur10e/physical_parameters.yaml'
   visual_parameters_file:='$(find ur_description)/config/ur10e/visual_parameters.yaml'
   transmission_hw_interface:=hardware_interface/PositionJointInterface
   safety_limits:=false
   safety_pos_margin:=0.15
   safety_k_position:=20"
  >
    <xacro:include filename="$(find ur_description)/urdf/inc/ur_macro.xacro"/>
    <xacro:ur_robot
      prefix="${prefix}"
      joint_limits_parameters_file="${joint_limits_parameters_file}"
      kinematics_parameters_file="${kinematics_parameters_file}"
      physical_parameters_file="${physical_parameters_file}"
      visual_parameters_file="${visual_parameters_file}"
      transmission_hw_interface="${transmission_hw_interface}"
      safety_limits="${safety_limits}"
      safety_pos_margin="${safety_pos_margin}"
      safety_k_position="${safety_k_position}"
    />

    <!-- initialize coupling link -->
    <link name="robotiq_coupling">
        <inertial>
        <origin xyz="8.625E-08 -4.6583E-06 0.03145" rpy="0 0 0" />
        <mass value="0.22652" />
        <inertia ixx="0.00020005" ixy="-4.2442E-10" ixz="-2.9069E-10" iyy="0.00017832" iyz="-3.4402E-08" izz="0.00013478" />
        </inertial>
        <visual>
        <origin xyz="0 0 0" rpy="0 0 0" />
        <geometry>
            <mesh filename="package://handling_unit_description/meshes/visual/coupling.dae" />
        </geometry>
        </visual>
    <collision>
        <origin xyz="0 0 0.004" rpy="0 0 0"/>
        <geometry>
        <cylinder radius="0.0375" length="0.014"/>
        </geometry>
    </collision>
    </link>


   <!-- Add camera mount -->
   <!--
    <link name="azure_kinect_mount">
        <inertial>
        <origin xyz="8.625E-08 -4.6583E-06 0.03145" rpy="0 0 0" />
        <mass value="0.22652" />
        <inertia ixx="0.00020005" ixy="-4.2442E-10" ixz="-2.9069E-10" iyy="0.00017832" iyz="-3.4402E-08" izz="0.00013478" />
        </inertial>
        <visual>
        <origin xyz="0 0 0" rpy="0 0 0" />
        <geometry>
            <mesh filename="package://handling_unit_description/meshes/visual/azure_kinect_mount.dae" />
        </geometry>
        </visual> --> 
    <!-- <collision>
        <origin xyz="0 0 0.004" rpy="0 0 0"/>
        <geometry>
          <cylinder radius="0.0375" length="0.014"/>
        </geometry>
    </collision> -->
    <!--
    </link>-->

    <!-- add fixed link to coupling -->
    <!--
    <joint name="kinect_mount_joint" type="fixed">
        <parent link="wrist_3_link" />
        <child link="azure_kinect_mount" />     -->
        <!-- <origin xyz="0 -0.045 -0.01725" rpy="${pi/2} ${-pi/2} 0" /> -->   <!-- connection of camera to wrist link-->
        <!--
        <origin xyz="0 -0.045 0.020" rpy="${pi/2} ${-pi/2} 0" /> --><!-- connection of camera at gripper link-->
    <!--
    </joint>

    <link name="azure_kinect_camera">
        <inertial>
        <origin xyz="8.625E-08 -4.6583E-06 0.03145" rpy="0 0 0" />
        <mass value="0.22652" />
        <inertia ixx="0.00020005" ixy="-4.2442E-10" ixz="-2.9069E-10" iyy="0.00017832" iyz="-3.4402E-08" izz="0.00013478" />
        </inertial>
        <visual>
          <origin xyz="0 0 0" rpy="0 0 0" />
          <geometry>
              <mesh filename="package://handling_unit_description/meshes/visual/azure_kinect.dae" />
          </geometry>
        </visual>
    <collision>
        <origin xyz="-0.00145 0 0.0065" rpy="0 0 0"/>
        <geometry>
          <box size="0.127 0.103 0.039" />
        </geometry>
    </collision>
    </link>
    -->


    
    <!-- add fixed joint between camera and camera mount-->
    <!--
    <joint name="kinect_joint" type="fixed">
        <parent link="azure_kinect_mount" />
        <child link="azure_kinect_camera" />
        <origin xyz="0 0 0.027" rpy="0 0 0" />
    </joint>
    -->
    <!-- add coupling to ur10e -->
    <joint name="gripper_coupling_joint" type="fixed">
        <parent link="tool0" />
        <child link="robotiq_coupling" />
        <origin xyz="0 0 0" rpy="0 0 0" />
    </joint>

    <!-- include and add robotiq 140 gripper -->
    <xacro:include filename="$(find robotiq_2f_140_gripper_visualization)/urdf/robotiq_arg2f_140_model.xacro"/>

    <joint name="gripper_joint" type="fixed">
        <parent link="robotiq_coupling" />
        <child link="robotiq_arg2f_base_link" />
        <origin xyz="0 0 0.008" rpy="0 0 ${pi/2}" />
    </joint>


    <!-- add tcp of gripper as link for planning -->
    <link name="gripper_tcp">
        <inertial>
          <origin xyz="8.625E-08 -4.6583E-06 0.03145" rpy="0 0 0" />
          <mass value="0" />
          <inertia ixx="0" ixy="0" ixz="0" iyy="0" iyz="0" izz="0" />
        </inertial>
        <visual>
          <origin xyz="0 0 0" rpy="0 0 0" />
        </visual>
    </link>

    <!-- add virtual joint to connect gripper_tcp with robotiq_arg2f_base_link so that gripper_tcp moves with the gripper -->
    <joint name="gripper_base_to_tcp_joint" type="fixed">
        <parent link="robotiq_arg2f_base_link" />
        <child link="gripper_tcp" />
        <origin xyz="0 0 0.185" rpy="0 0 0" />
    </joint>


    <xacro:include filename="$(find robotiq_2f_140_gripper_visualization)/urdf/robotiq_arg2f_transmission.xacro"/>
  </xacro:macro>
</robot>